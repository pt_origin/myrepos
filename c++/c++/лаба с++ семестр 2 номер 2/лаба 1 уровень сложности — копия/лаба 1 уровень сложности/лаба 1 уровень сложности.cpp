// ���� 1 ������� ���������.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#include "stdafx.h"
#include <iostream>
#include <iomanip>

using namespace std;

void func_substitution(double ** a, const int nstr, const int nstb)
{
	for ( int i = 0 ; i < nstr ; i++)
	{
		for( int j = 0 ; j < nstb ; j++)
		{
			if ( a [i][j] > 0 )
				a[i][j] = 1;
			else if ( a[i][j] < 0) 
				a[i][j] = -1;
			else a[i][j] = 0;
			
		}
	}
}



int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL,"Russian");
	int row,col;
	cout << "������� ����������� ������� " << endl;
	cin >> row >> col;
	double **d_arr = new double* [row];
	for (int i = 0; i < row;i++) 
	{
		d_arr[i] = new double [col];
	}
		

	cout << "������� �������� ������� " << endl;
	for ( int i = 0 ; i < row ; i++)
	{
		for (int j = 0 ; j < col ; j++)
		{
			cin >> d_arr[i][j];
		}
	}
	func_substitution(d_arr, row, col);
	cout <<" ���������� ������� " << endl;
	for ( int i = 0 ; i < row ; i++)
	{
		for (int j = 0 ; j < col ; j++)
		{
			cout << setw(4) << d_arr[i][j];
		}
		cout << endl;
	}

	for (int i = 0; i < row;i++) 
	{
		delete [] d_arr[i] ; 
	} 
	delete d_arr;

	system("pause");
	return 0;
}

