// ���� sem02�1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream> 
#include <cmath>
#include <iomanip>

using namespace std;

double (*pt2Func)(double ) = NULL;

double metod_iter ( double start , double finish , double &eps , int max_iter , double (*pt2Func)(double ) , int &quantity_iter  ){
	double x0 = start;// ��������� ���������� �� ������� ������� 
	double x1 = NULL;
	do{
		x1 = (*pt2Func)(x0);
		quantity_iter++;
		if ( abs(x1 - x0) < eps || quantity_iter == max_iter )
			break;
		x0 = x1;


	}while ( true ) ;

	return x1;
}
void proverka_var_a (double rez){//����������� ���������� �������� 
	if ( (abs((0.4 + atan ( sqrt (rez)) - rez))  == 0  ) )
		cout << "�������� ������,��� ����������� � ��������� �� ����� 0" << endl;
	else cout << "�������� �� ������, ����������� " << (0.4 + atan ( sqrt (rez)) - rez) << " �� ����� 0 " << endl ;
		
}

void proverka_var_b (double rez){//����������� ���������� �������� 
	if ( (rez*rez*rez - rez - 1.1 )  == 0  ) 
		cout << "�������� ������,��� ����������� � ��������� �� ����� 0" << endl;
	else cout << "�������� �� ������, ����������� � " << rez*rez*rez - rez - 1.1 << " �� 0 " << endl ;
		
}

double var_a(double x){
	double x1 = 0.4 + atan ( sqrt (x) );
	return x1;
}

double var_b(double z){
	

	double z1 = pow(z + 1.1, 1.0/3);
	
		return z1;
	//https://www.wolframalpha.com/input/?i=x*x*x-x-1.1%3D0
}

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");
	int quantity_iter = 0;
	double eps = 0.0001; double start,finish;
	int max_iter;
	//�������������� ���������:

	//pt2Func = &MyFunction;
	/*double x1,x0;
	
	x0 = 0;
	do{
		x1 = 0.4 + atan ( sqrt (x0));
		if ( abs(x1 - x0) < eps )
			break;
		x0 = x1;


	}while ( true ) ;
	cout << x1;*/
	char choise ;
	cout << "������� ����� ������� ��� ����� a ��� b" << endl;
	cin >> choise;
	double rez;
	if (choise == 'a'){
		cout << " ������� ����� ������ ���������  � �������� �������� ����������, ��������, � ��� �� ����������� ���������� ���������� �������� " << endl;
		cin >> start >> finish >> eps >> max_iter;
		pt2Func = &var_a;
		rez = metod_iter(start , finish, eps , max_iter , pt2Func , quantity_iter);
		cout << "���������� ������ "<< setprecision(5) << rez << " . ���������� ����� " << quantity_iter << endl;
		proverka_var_a(rez);
	}
	else {cout << " ������� ����� ������ ���������  � �������� �������� ����������, �������� , � ��� �� ����������� ���������� ���������� �������� " << endl;
		cin >> start >> finish >> eps >> max_iter;
		pt2Func = &var_b;
		rez = metod_iter(start , finish, eps , max_iter , pt2Func , quantity_iter);
		cout << "���������� ������ "<< setprecision(5) <<rez << " . ���������� ����� " << quantity_iter << endl;
		proverka_var_b(rez);


		
	}

	

	system("pause");

	return 0;
}

