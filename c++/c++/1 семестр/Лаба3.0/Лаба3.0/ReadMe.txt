﻿========================================================================
    КОНСОЛЬНОЕ ПРИЛОЖЕНИЕ. Обзор проекта Лаба3.0
========================================================================

Это приложение Лаба3.0 создано автоматически с помощью мастера 
приложений.

Здесь приведены краткие сведения о содержимом каждого из файлов, использованных 
при создании приложения Лаба3.0.


Лаба3.0.vcxproj
    Основной файл проекта VC++, автоматически создаваемый с помощью мастера 
    приложений.
    Он содержит данные о версии языка Visual C++, использованной для создания 
    файла, а также сведения о платформах, настройках и свойствах проекта, 
    выбранных с помощью мастера приложений.

Лаба3.0.vcxproj.filters
    Это файл фильтров для проектов VC++, созданный с помощью мастера 
    приложений. 
    Он содержит сведения о сопоставлениях между файлами в вашем проекте и 
    фильтрами. Эти сопоставления используются в среде IDE для группировки 
    файлов с одинаковыми расширениями в одном узле (например файлы ".cpp" 
    сопоставляются с фильтром "Исходные файлы").

Лаба3.0.cpp
    Это основной исходный файл приложения.

/////////////////////////////////////////////////////////////////////////////
Другие стандартные файлы:

StdAfx.h, StdAfx.cpp
    Эти файлы используются для построения файла предкомпилированного заголовка 
    (PCH) с именем Лаба3.0.pch и файла предкомпилированных типов 
    с именем StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Общие замечания:

С помощью комментариев «TODO:» в мастере приложений обозначаются фрагменты 
исходного кода, которые необходимо дополнить или изменить.

/////////////////////////////////////////////////////////////////////////////
