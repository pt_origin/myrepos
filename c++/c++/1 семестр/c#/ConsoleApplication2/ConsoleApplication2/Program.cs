﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1, s2; // переменные типа строка,литерные

            string surname = "Шульженко";
            string name = "Олег";
            string otchestvo = "Васильевич";

            int age = 40;
            double weight = 88.73;

            s1 = surname + " " + name + " " + otchestvo + ", возраст " + age + ", вес " + weight;

            surname = "Чугунов";
            name = "Александр";
            otchestvo = "Игоревич";

            age = 23;
            weight = 66;

            s2 = surname + " " + name + " " + otchestvo + ", возраст " + age + ", вес " + weight;

            System.Console.WriteLine(s1);
            System.Console.WriteLine(s2);

            System.Console.ReadLine();

        }
    }
}
