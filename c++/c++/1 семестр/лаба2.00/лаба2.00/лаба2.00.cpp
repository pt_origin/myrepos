// ����2.00.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#include <iostream>
using namespace std;
typedef unsigned long long int ull;
 
int main() {
    auto f = [](ull n, ull m, const auto& p, const auto& la) -> ull {
        return n ? la(n/10, p(n%10) ? m*10+n%10 : m, p, la) : m;};
    
    ull n; cout<<"Input n: "; cin>>n; cout<<"Rezult = ";    
    cout<<f(f(n, 0, [](int n) {return true;}, f), 0, [](int n) {return n!=3 && n!=7;}, f)<<'\n';
    return 0;
}