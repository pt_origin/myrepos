// lab rab 2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	double xk1, yk1, xk, yk, eps;
	cout<< "Input xk1, yk1, eps " << endl;
	cin>>xk1>>yk1>>eps;
	xk=0;
	yk=0;
	int k;
	k=1;

	do
	{ xk=(xk1+yk1)/2;
	  yk=sqrt(yk1*xk1);
	  xk1=xk;
	  yk1=yk;
	  k++;


	}while (abs(xk-yk)<!eps);
	cout<< "xk="<< xk<< endl;
	cout<< "k="<< k<< endl;

	system("pause");
	return 0;
}

