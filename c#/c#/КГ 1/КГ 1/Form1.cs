﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace КГ_1
{
    public partial class Form1 : Form
    {
        Graphics myGraph;
        Bitmap myBit;

        Point CarPoint1 = new Point();
        Point CarPoint2 = new Point();
        Point CarPoint3 = new Point();
        Point CarPoint4 = new Point();
        Point CarPoint5 = new Point();
        Point CarPoint6 = new Point();
        Point CarPoint7 = new Point();
        Point CarPoint8 = new Point();
        int t = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            Draw_Background(myGraph);
            Draw_Car(myGraph,t);
            t++;
            if (t > 550)
                t = 0;
            //t++;
            
            pictureBox1.Image = myBit;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            myBit = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            myGraph = Graphics.FromImage(myBit);
            myGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            timer1.Start();



        }
        private void Draw_Background (Graphics graphics)
        {
            double h = pictureBox1.DisplayRectangle.Height;
            double w = pictureBox1.DisplayRectangle.Width;
            graphics.Clear(Color.SkyBlue);
            graphics.FillRectangle(Brushes.Green, pictureBox1.DisplayRectangle.X, pictureBox1.DisplayRectangle.Y + pictureBox1.DisplayRectangle.Height * 7 / 17, pictureBox1.DisplayRectangle.Width, pictureBox1.DisplayRectangle.Height*10/17);
            graphics.FillRectangle(Brushes.Gray, pictureBox1.DisplayRectangle.X, pictureBox1.DisplayRectangle.Y + pictureBox1.DisplayRectangle.Height * 9 / 17, pictureBox1.DisplayRectangle.Width, pictureBox1.DisplayRectangle.Height * 4 / 17);
            graphics.FillEllipse(Brushes.Yellow, pictureBox1.DisplayRectangle.Width / 4, pictureBox1.DisplayRectangle.Height/2 - 150, 50, 50);
            for (int i = 0; i < 9; i++)
            {
                graphics.FillRectangle(Brushes.Brown, (float)(pictureBox1.DisplayRectangle.X + 15.06*h/100 + i*70), pictureBox1.DisplayRectangle.Y + pictureBox1.DisplayRectangle.Height * 9 / 17 - pictureBox1.DisplayRectangle.Height * 2 / 34 - 4, pictureBox1.DisplayRectangle.Width * 1 / 20, pictureBox1.DisplayRectangle.Height * 2 / 34);
                graphics.FillEllipse(Brushes.DarkGreen, (float)(pictureBox1.DisplayRectangle.X + 11.78*h/100 + i*70), pictureBox1.DisplayRectangle.Y + pictureBox1.DisplayRectangle.Height * 3 / 17 - 4, 60, 120);
            }
            }


        private void Draw_Car(Graphics graphics,int i)
        {
            Point cp = new Point();
            
            double h = pictureBox1.DisplayRectangle.Height;
            double w = pictureBox1.DisplayRectangle.Width;
            CarPoint1 = new Point(0 + i, 230);
            CarPoint2 = new Point(0 + i, 200);
            CarPoint3 = new Point(25 + i, 200);
            CarPoint4 = new Point(25 + i, 170);
            CarPoint5 = new Point(95 + i, 170);
            CarPoint6 = new Point(95 + i, 200);
            CarPoint7 = new Point(150 + i, 200);
            CarPoint8 = new Point(150 + i, 230);
            Point[] CarPoints = { CarPoint1, CarPoint2, CarPoint3, CarPoint4, CarPoint4, CarPoint5, CarPoint6, CarPoint7, CarPoint8 };
            graphics.FillPolygon(Brushes.Red, CarPoints);
            graphics.FillRectangle(Brushes.DeepSkyBlue, 65 + i, 173, 27, 27);
            graphics.FillRectangle(Brushes.DeepSkyBlue, 30 + i, 173, 27, 27);
            graphics.FillEllipse(Brushes.Black,5 + i, 215, 25, 25);
            graphics.FillEllipse(Brushes.Black, 120 + i, 215, 25, 25);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            t = 0;
            myBit = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            myGraph = Graphics.FromImage(myBit);
            myGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            timer1.Start();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }
    }
}
