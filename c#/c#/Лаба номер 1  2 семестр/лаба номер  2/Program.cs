﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace лаба_номер__2
{
    public enum Education
    {
        Specialist = 0,
        Вachelor = 1,
        SecondEducation = 2
    }
    class Program
    {
        static void Main(string[] args)
        {
            StudentCollection SC1 = new StudentCollection();
            StudentCollection SC2 = new StudentCollection();
            Journal MyJournal1 = new Journal();
            Journal MyJournal2 = new Journal();
            SC1.StudentsCountChanged += MyJournal1.StudentsCountChangedEventHandler;
            SC1.StudentReferenceChanged += MyJournal1.StudentsReferenceChangedEventHandler;
            SC1.StudentReferenceChanged += MyJournal2.StudentsReferenceChangedEventHandler;
            SC2.StudentReferenceChanged += MyJournal2.StudentsReferenceChangedEventHandler;

            SC1.AddDefaults();
            SC1.Remove(2);
            SC1[1] = new Student(new Person(), 0, 28);

            SC2.AddDefaults();
            SC2.AddDefaults();
            SC2.Remove(4);
            SC2[4] = new Student(new Person(), 0, 28);

            Console.WriteLine(MyJournal1.ToString());
            Console.WriteLine(MyJournal2.ToString());

            Console.ReadLine();
        }
    }
}
