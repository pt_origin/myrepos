﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_номер__2
{
    public class JournalEntry
    {
        public string NameCollection { get; set; }
        public string ChangeInCollection { get; set; }
        public Student ReferenceOnStudent { get; set; }

        public JournalEntry()
        {
            NameCollection = "";
            ChangeInCollection = "";
            ReferenceOnStudent = new Student();
        }
        public JournalEntry(string NameCollection, string ChangesInCollection, Student ReferenceOnStudent)
        {
            this.NameCollection = NameCollection;
            this.ChangeInCollection = ChangesInCollection;
            this.ReferenceOnStudent = ReferenceOnStudent;
        }
        public override string ToString()
        {
            string str = "";
            str = "\n Событие произошло в " + NameCollection.ToString();
            str = str + "\n Тип изменений: " + ChangeInCollection + "\n Со студентом" + ReferenceOnStudent.ToString();
            return str;
        }
    }
}