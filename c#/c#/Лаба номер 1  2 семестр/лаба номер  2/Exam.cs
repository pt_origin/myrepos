﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_номер__2
{
    public class Exam : IDateAndCopy
    {
        //private int p_mark;
        //private string p_name;
        //private System.DateTime p_date_time;
        public int Mark
        {
            get;
            set;
        }
        public string Name
        {
            get
            ;
            set
           ;
        }
        public System.DateTime DateExam
        {
            get;
            set;

        }
        public Exam()
        {
            this.Name = "Unknown";
            this.Mark = 2;
            this.DateExam = System.DateTime.Now;
        }
        public Exam(int p_mark, string p_name, System.DateTime date_time)
        {
            this.Name = p_name;
            this.Mark = p_mark;
            this.DateExam = date_time;
        }
        public Exam(int p_mark)
        {
            this.Mark = p_mark;
        }
        public Exam(System.DateTime p_date_time)
        {
            this.DateExam = p_date_time;
        }
        public Exam(string p_name)
        {
            this.Name = p_name;
        }
        public override string ToString()
        {
            return string.Format("Name of exam = " + this.Name + "\n" + "Mark = " + this.Mark + "\n" + "Date Exam = " + this.DateExam + "\n");
            
        }
        public override bool Equals(object obj)
        {
            if (obj is Exam)
            {
                Exam temp = obj as Exam;
                return this.ToString() == temp.ToString();
            }
            else return false;
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        public static bool operator ==(Exam p1, Exam p2)
        {
            return p1.Equals(p2);
        }
        public static bool operator !=(Exam p1, Exam p2)
        {
            return !p1.Equals(p2);
        }
        public object DeepCopy()
        {
            return new Exam(this.Mark, this.Name, this.DateExam);
        }
        public DateTime Date
        {
            get
            {
                return this.DateExam;
            }
            set
            {
                this.DateExam = value;
            }
        }
        
    }
    
}

