﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAba1_csharp_hrest_nolik
{
    class TicTacToe
    {
        static bool Win = false;
        static void Game(uint n, string Pl1, string Pl2)
        {
            char[,] XO = new char[n, n];
            char check;
            uint x, y, move = 1;
            for (int i = 0; i < XO.GetLength(0); i++)
            {
                for (int j = 0; j < XO.GetLength(1); j++)
                {
                    XO[i, j] = '-';
                }
            }
            Drawing(XO);
            while (!Win)
            {
                if (move % 2 != 0) Console.WriteLine("Ход игрока " + Pl1);
                else Console.WriteLine("Ход игрока " + Pl2);
                bool WrongMove = true;
                while (WrongMove)
                {
                    x = PointsTest(n);
                    y = PointsTest(n);
                    x--; y--;
                    WrongMove = Logic(ref XO, x, y, move);
                }
                Console.Clear();
                Console.WriteLine();
                for (int i = 0; i < XO.GetLength(0); i++)
                {
                    for (int j = 0; j < XO.GetLength(1); j++)
                    {
                        Console.Write("{0,4}", XO[i, j]);
                    }
                    Console.WriteLine();
                    Console.WriteLine();
                }
                if (move % 2 != 0)
                {
                    check = 'X';
                    Win = WinningTest(ref XO, Pl1, n, check);
                }
                else
                {
                    check = 'O';
                    Win = WinningTest(ref XO, Pl2, n, check);
                }
                move++;
            }
        }
        static bool WinningTest(ref char[,] XO, string name, uint size, char ch)
        {
            uint k = 0, l = 0, m = 0, p = 0;
            uint F = size;
            F--;
            for (int i = 0; i < XO.GetLength(0); i++)
            {
                k = 0; 
                for (int j = 0; j < XO.GetLength(1); j++)
                {
                    if (XO[i, j] == ch) k++;
                    //if (XO[i, F] == ch) p++;
                }
                if (k == size)
                    break;
            }
            for (int i = 0; i < XO.GetLength(0); i++)
            {
                l = 0; m = 0;
                for (int j = 0; j < XO.GetLength(1); j++)
                {
                    if (XO[i, j] == ch) l++;
                    if (XO[j, i] == ch) m++;
                }
            }

            if (k == size | l == size | m == size | p == size)
            {
                Console.WriteLine("Игрок {0} победил!", name);
                return true;
            }
            else return false;
        }
        static void Drawing(char[,] field)
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    Console.Write("{0,4}", field[i, j]);
                }
                Console.WriteLine();
                Console.WriteLine();
            }
        }
        static bool Logic(ref char[,] XO, uint X, uint Y, uint MoveC)
        {
            if (XO[X, Y] == '-')
            {
                if (MoveC % 2 == 0)
                {
                    XO[X, Y] = 'O';
                }
                else
                {
                    XO[X, Y] = 'X';
                }
                return false;
            }
            else
            {
                Console.WriteLine("Это место уже занято! Введите другие координаты.");
                return true;
            }
        }
        static uint PointsTest(uint lim)
        {
            uint k = 0;
            bool T = true;
            while (T)
            {
                try
                {
                    k = Convert.ToUInt32(Console.ReadLine());
                    if (k <= lim)
                    {
                        T = false;
                    }
                    else Console.WriteLine("Вы вышли за пределы поля. Попробуйте еще раз.");
                }
                catch (FormatException)
                {
                    Console.WriteLine("Ошибка! Повторите ввод.");
                }
            }
            return k;
        }
        static uint InputTest()
        {
            uint k = 0;
            bool T = true;
            while (T)
            {
                try
                {
                    k = Convert.ToUInt32(Console.ReadLine());
                    T = false;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Ошибка! Повторите ввод.");
                }
            }
            return k;
        }
        static void Main(string[] args)
        {
            string name1, name2;
            uint Fsize;
            Console.WriteLine("Введите имя игрока №1: ");
            name1 = Console.ReadLine();
            Console.WriteLine("Введите имя игрока №2: ");
            name2 = Console.ReadLine();
            Console.WriteLine("Введите размер поля: ");
            Fsize = InputTest();
            Console.Clear();
            Console.WriteLine();
            Game(Fsize, name1, name2);
            Console.ReadLine();
            Console.Clear();
        }
    }
}
