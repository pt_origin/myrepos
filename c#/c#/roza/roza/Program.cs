﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2сем__1__2_
{
    class Program
    {
        static int napr;
        static int kolvobroskov;
        static void Main(string[] args)
        {
            // int kolvobroskov;
            string t = "";
            string m = "12";
            Console.WriteLine("Колличество бросков у двух игроков одинаково. Введите это значение: ");
            kolvobroskov = Convert.ToInt32(Console.ReadLine());

            //ввод поля
            string[,] pole = new string[6, 6];
            for (int i = 0; i < pole.GetLength(0); i++)
            {
                for (int j = 0; j < pole.GetLength(1); j++)
                {

                    pole[i, j] = "*";

                }
            }
            Console.WriteLine("pole: ");
            pole[5, 0] = "12";
            for (int i = 0; i < pole.GetLength(0); i++)
            {
                Console.WriteLine();
                Console.WriteLine();
                for (int j = 0; j < pole.GetLength(1); j++)
                {
                    Console.Write("    ");
                    Console.Write(pole[i, j]);
                }
            }
            //pole[0, 5] = t;
            //pole[0, 5]=m;
            //m=t;

            Console.WriteLine();
            Console.WriteLine();

            while (kolvobroskov > 0)
            {
                //Console.WriteLine("Осталось бросков: ");
                Console.WriteLine("Осталось бросков: " + kolvobroskov);
                Console.WriteLine("Ход 1-го игрока: ");
                Console.WriteLine();
                Igrok1(pole);
                //рисуем
                Console.WriteLine("Ход 2-го игрока: ");
                Console.WriteLine();
                Igrok2(pole);
                //рисуем
                //проверяем победа 1 или победа 2 или победа 1,2 ли никто не дошел
                kolvobroskov--;
            }
            Console.ReadLine();
        }


        public static void Napravleniye(string[,] f1x)
        {
            for (int i = 0; i <= 6; i++)
            {
                for (int j = 6; j >= 0; j--)
                {
                    if ((i == 6 && j <= 6 && j >= 1) || (i == 5 && j <= 5 && j >= 2) || (i == 4 && j <= 4 && j >= 3))
                    {
                        napr = 1;//вверх
                    }
                    else if ((i == 0 && j <= 4 && j >= 0) || (i == 1 && j <= 3 && j >= 1) || (i == 2 && j == 2))
                    {
                        napr = 2;//вниз
                    }
                    else if ((j == 5 && i <= 4 && i >= 0) || (j == 4 && i <= 3 && i >= 1) || (j == 3 && i == 2))
                    {
                        napr = 3;//вправо
                    }
                    else if ((j == 0 && i <= 6 && i >= 1) || (j == 1 && i <= 5 && i >= 2) || (j == 2 && i <= 4 && i >= 3))
                    {
                        napr = 4;//влево
                    }
                }
            }
        }

        public static void Proverka()
        {

        }
        public static void Drawing()
        {

        }

        public static void Igrok1(string[,] f1)
        {
            //int napr;//будет использовано значение napr из функции Napravleniye
            Napravleniye(f1);
            Random r1 = new Random();
            bool b = false;
            int ost = 0;
            int k1 = 0, k2 = 0, k3 = 0, k4 = 0, obshiydiapazon = 6;
            int m1, m2;
            int mesto1;
            int ni, nj;

            for (int i = 0; i < f1.GetLength(0); i++)
            {
                for (int j = f1.GetLength(1); j > 0; j--)
                {
                    switch (napr)
                    {
                        case 1:
                            j = j - r1.Next(1, 7) - ost;
                            if (j < obshiydiapazon - k1)
                            {
                                ost = j - (obshiydiapazon - k1);
                                k1++;
                            }
                            else ost = 0;
                            break;
                        case 2:
                            j = j + r1.Next(1, 7) + ost;
                            m1 = r1.Next(1, 7) + ost;
                            if (j > obshiydiapazon - 1 - k2)
                            {
                                ost = m1 - (obshiydiapazon - 1 - k2);
                                k2++;
                            }
                            else ost = 0;
                            break;
                        case 3:
                            i = i + r1.Next(1, 7) + ost;
                            if (i > obshiydiapazon - k3)
                            {
                                ost = i - (obshiydiapazon - k3);
                                k3++;
                            }
                            else ost = 0;
                            break;
                        case 4:
                            i = i - r1.Next(1, 7) - ost;
                            m2 = r1.Next(1, 7) + ost;
                            if (i < obshiydiapazon - k4)
                            {
                                ost = m2 - (obshiydiapazon - k4);
                                k4++;
                            }
                            else ost = 0;
                            break;
                        default:
                            Console.WriteLine("Произошла ошибка.");
                            break;
                    }
                    b = true;
                }
                if (b == true)
                {
                    break;
                }
            }
        }



        public static void Igrok2(string[,] f2)
        {
            //int napr;//будет использовано значение napr из функции Napravleniye
            Napravleniye(f2);
            Random r1 = new Random();
            int ost = 0;
            int k1 = 0, k2 = 0, k3 = 0, k4 = 0, obshiydiapazon = 6;
            int m1, m2;
            bool b = false;


            for (int i = 0; i <= 6; i++)
            {
                for (int j = 6; j >= 0; j--)
                {
                    switch (napr)
                    {
                        case 1:
                            j = j - r1.Next(1, 7) - ost;
                            if (j < obshiydiapazon - k1)
                            {
                                ost = j - (obshiydiapazon - k1);
                                k1++;
                            }
                            else ost = 0;
                            break;
                        case 2:
                            j = j + r1.Next(1, 7) + ost;
                            m1 = r1.Next(1, 7) + ost;
                            if (j > obshiydiapazon - 1 - k2)
                            {
                                ost = m1 - (obshiydiapazon - 1 - k2);
                                k2++;
                            }
                            else ost = 0;
                            break;
                        case 3:
                            i = i + r1.Next(1, 7) + ost;
                            if (i > obshiydiapazon - k3)
                            {
                                ost = i - (obshiydiapazon - k3);
                                k3++;
                            }
                            else ost = 0;
                            break;
                        case 4:
                            i = i - r1.Next(1, 7) - ost;
                            m2 = r1.Next(1, 7) + ost;
                            if (i < obshiydiapazon - k4)
                            {
                                ost = m2 - (obshiydiapazon - k4);
                                k4++;
                            }
                            else ost = 0;
                            break;
                        default:
                            Console.WriteLine("Произошла ошибка.");
                            break;
                    }

                }
            }

        }
    }
}
