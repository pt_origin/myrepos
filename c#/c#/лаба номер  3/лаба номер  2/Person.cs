﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_номер__2
{
    public class Person : IDateAndCopy
    {
        protected string firstname;
        protected string surname;
        protected System.DateTime birthday;

        public string FirstName
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
            }
        }
        public string SurName
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }
        public DateTime Birthday
        {
            get
            {
                return birthday;
            }
            set
            {
                birthday = value;
            }
        }
        public int YearBirth
        {
            get
            {
                return birthday.Year;
            }
            set
            {
                birthday = new DateTime(value, birthday.Month, birthday.Day);
            }
        }
        public Person(string name, string surname, DateTime birthday)
        {
            FirstName = name;
            SurName = surname;
            Birthday = birthday;
        }
        public Person()
        {
            FirstName = "Unknown FirstName";
            SurName = "Unknown SurName";
            Birthday = DateTime.Today;
        }
        public override string ToString()
        {

            return "Name of person= " + this.FirstName + "\n" + "Surname = " + this.SurName + "\n" + "Birthday = " + this.Birthday;

        }
        public virtual string ToShortString()
        {
            return string.Format("Name of person= " + this.FirstName + "\n" + "Surname = " + this.SurName + "\n");

        }
        public override bool Equals(object obj)
        {
            if (obj is Person)
            {
                Person temp = obj as Person;
                return this.ToString() == temp.ToString();
            }
            else return false;
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        public static bool operator ==(Person p1, Person p2)
        {
            return p1.Equals(p2);
        }
        public static bool operator !=(Person p1, Person p2)
        {
            return !p1.Equals(p2);
        }
        public virtual object DeepCopy()
        {
            return new Person(this.FirstName, this.SurName, this.Birthday);
        }
        public DateTime Date
        {
            get
            {
                return this.Birthday;
            }
            set
            {
                this.Birthday = value;
            }
        }
    }
    
}
