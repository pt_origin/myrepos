﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace лаба_номер__2
{
    public enum Education
    {
        Specialist = 0,
        Вachelor = 1,
        SecondEducation = 2
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Student MyStudent = new Student();
            //Console.WriteLine(MyStudent.ToString());
            Person P1 = new Person("Vasya", "Petrov", new DateTime(2000, 12, 5));
            Person P2 = new Person("Vasya", "Petrov", new DateTime(2000, 12, 5));
            Console.WriteLine("Hash for p1 = " + P1.GetHashCode() + "\n" + "Hash for p2 = "+ P2.GetHashCode() + "\n");
            P2.FirstName = "Not Vasya ";
            Console.WriteLine(P1);
            Console.WriteLine(P2);
            
            Student MyStudent = new Student();
            Exam[] ex = {
                             new Exam(92,"Основы программирования",new DateTime(2018,6,18)),
                             new Exam(92,"Линейная Алгебра",new DateTime(2018,6,21)),
                             new Exam(90,"Матанализ", new DateTime(2018,6,25)),
                             new Exam(98,"Дискретная математика",new DateTime(2018,6,30))
                         };
            MyStudent.AddExams(ex);
            Test[] ts = {
                            new Test("История",true),
                            new Test(),
                            new Test("English" ,true)
                        };
            MyStudent.AddTests(ts);
            Console.WriteLine("значение свойства типа Person для объекта типа Student " + "\n" + MyStudent.Person.ToString());
            Console.WriteLine(MyStudent.ToString());
            object obj = MyStudent.DeepCopy();
            Student MyStudent2 = obj as Student;
            MyStudent.FirstName = "Pavlo";
            MyStudent.SurName = "Tsipocshuk";
            MyStudent.Birthday = new DateTime(2000, 5, 12);
            MyStudent.NumberGroup = 12;
            MyStudent.myExam[0].Name = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
            MyStudent.myTest[0] = new Test("Комп. графика ", true);
            Console.WriteLine("Changed student");
            Console.WriteLine(MyStudent.ToString());
            Console.WriteLine("Not-changed DeepCopy of student ");
            Console.WriteLine(MyStudent2.ToString());
            try
            {
                MyStudent2.NumberGroup = 120;
            }
            catch (InvalidOperationException exx) 
            {
                Console.WriteLine("Ошибка "+exx.Message);
  
            }
            Console.WriteLine("Все зачеты и экзамены ");
            foreach (var v in MyStudent2.GetAllIterator())
                Console.WriteLine(v);
            Console.WriteLine("Экзамены выше 92 баллов");
            foreach(var v in MyStudent2.GetExamIterator(92))
                Console.WriteLine(v);
            Console.ReadLine();
        }
    }
}
