﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_номер__2
{
    public class Test
    {
        public string NameExam { get; set; }
        public bool PassExam { get; set; }

        public Test()
        {
            NameExam = "NameTest";
            PassExam = true;
        }
        public Test(string name, bool pe)
        {
            NameExam = name;
            PassExam = pe;
        }
        public override string ToString()
        {
            return string.Format("NameTest :" + NameExam + "\n" + "Pass :" + PassExam + "\n");
        }
    }
}
