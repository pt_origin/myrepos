﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public class Journal
    {
        private System.Collections.Generic.List<JournalEntry> JournalEntries;
        public void StudentsCountChangedEventHandler(object source, StudentListHandlerEventArgs args)
        {
            JournalEntries.Add(new JournalEntry(args.NameCollection, args.ChangesInCollection, args.ReferenceOnStudent));
        }
        public void StudentsReferenceChangedEventHandler(object source, StudentListHandlerEventArgs args)
        {
            JournalEntries.Add(new JournalEntry(args.NameCollection, args.ChangesInCollection, args.ReferenceOnStudent));
        }
        public Journal()
        {
            JournalEntries = new List<JournalEntry>();
        }
        public override string ToString()
        {
            string str = "\n Журнал";
            for (int i = 0; i < JournalEntries.Count; i++)
            {
                str = str + "\n" +  i.ToString() + "-ая запись в журнале " + JournalEntries[i].ToString() + "=======================" ;
            }
            return str;
        }
    }
}