﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public class HelpClass :IComparer<Student>
    {
        int IComparer<Student>.Compare(Student obj1, Student obj2)
        {
            Student temp1 = obj1 as Student;
            Student temp2 = obj2 as Student;
            if (temp1 != null && temp2 != null)
            {
                return temp1.AverageMarks.CompareTo(temp2.AverageMarks);
            }
            else throw new ArgumentException("Param is not Student ");
        }
    }
}
