﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public class StudentsChangedEventArgs<Tkey> : EventArgs
    {
        public string NameCollection{get;set;}
        public Action GetAction{ get; set; }
        public string GetSender { get; set; }
        public Tkey GetKey { get; set; }
        public StudentsChangedEventArgs(string namecollection, Action action, string sender,Tkey key)
        {
            NameCollection = namecollection;
            GetAction = action;
            GetSender = sender;
            GetKey = key;
        }
        public override string ToString()
        {
            string str = "Имя коллекции" + NameCollection + "\n" + GetAction.ToString() + "\n" + "Источник изменения" + GetSender + "\n" + GetKey + "\n";
            return str;
        }
    }
}