﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public class StudentCollection <Tkey>
    {
        private System.Collections.Generic.List<Student> Group;
        public string NameCollection { get; set; }
        public delegate void StudentListHandler(object source, StudentListHandlerEventArgs args);
        public delegate void StudentsChangedHandler<TKey>(object source,StudentsChangedEventArgs<TKey> args);

        public bool Remove(Student st)
        {
            if (j >= Group.Count && j < 0)
                return false;
            StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Удален элемент из колекции", Group[j]));
            Group.RemoveAt(j);

            return true;
        }
        public Student this[int index]
        {
            get
            {
                if (index >= Group.Count && index < 0 )
                {
                    Console.WriteLine("Вы пытаетесь обратиться к элементу с несуществующим индексом");
                    return null;
                }
                return Group[index];
            }
            set
            {
                if (index >= Group.Count && index < 0)
                {
                    Console.WriteLine("Вы пытаетесь обратиться к элементу с несуществующим индексом");
                    return;
                }
                if(StudentReferenceChanged == null)
                    Console.WriteLine(" 0 Subscribers");
                StudentReferenceChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Изменен элемент в колекцию", value));
                Group[index] = value;
                
            }

        }

        public event StudentListHandler StudentsCountChanged;
        public event StudentListHandler StudentReferenceChanged;

        public StudentCollection()
        {
            NameCollection = "Имя коллекции";
            Group = new List<Student> ();
        }
        public void AddDefaults()
        {
            Student NewSt = new Student();
            Group.Add(NewSt);
            StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", NewSt));
            //StudentsCountChanged(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", NewSt));
            NewSt = new Student(new Person("StudentName", "StudentSurName", new DateTime(2000, 1, 1)), 0, 1);
            Group.Add(NewSt);
            StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", NewSt));
            NewSt = new Student(new Person("StudentName2", "StudentSurName2", new DateTime(2000, 2, 2)), 0, 1);
            Group.Add(NewSt);
            StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", NewSt));


        }
        public void AddStudents(params Student[] group)
        {
            for (int i = 0; i < group.Length; i++)
            {
                Group.Add(group[i]);
                StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", group[i]));
            }
        }

        public override string ToString()
        {
            string str;
            str = "Group\n";
            for (int i = 0; i < Group.Count; i++)
            {
                str += Group[i].ToString();
            }
            return str;
        }
        public  string ToShortString()
        {
            string str;
            str = "Group\n";
            for (int i = 0; i < Group.Count; i++)
            {
                str += Group[i].ToShortString();
            }
            return str;
        }
        public void SortBySurName()
        {
            Group.Sort();  
        }
        public void SortByBirthday()
        {
            Group.Sort(new Person());
        }
        public void SortByMarks()
        {
            Group.Sort(new HelpClass());
        }



    }
}
