﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public class StudentListHandlerEventArgs : EventArgs
    {
        public string NameCollection { get; set; }
        public string ChangesInCollection { get; set; }
        public Student ReferenceOnStudent { get; set; }
        public StudentListHandlerEventArgs()
        {
            NameCollection = "";
            ChangesInCollection = "";
            ReferenceOnStudent = new Student();
        }
        public StudentListHandlerEventArgs(string NameCollection,string ChangesInCollection,Student ReferenceOnStudent)
        {
            this.NameCollection = NameCollection;
            this.ChangesInCollection = ChangesInCollection;
            this.ReferenceOnStudent = ReferenceOnStudent;
           
        }

        public override string ToString()
        {
            string str = "";
            str = "\n Событие произошло в " + NameCollection.ToString();
            str = str + "\n Тип изменений: " + ChangesInCollection + "\n Со студентом" + ReferenceOnStudent.ToShortString();
            return str;
        }
    }
}