﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Алгоритмы_и_структуры_данных
{
    public partial class Form1 : Form
    {
        MyDict [] dictoanary = null;
        MyDict [] dictoanaryCopy = null;
        
        int Count;


        public Form1()
        {
            InitializeComponent();
            chart1.Series.Add("Кол-во перестановок");
            chart1.Series.Add("Кол-во сравнений");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(File.ReadAllText(@"Шапка.txt", Encoding.GetEncoding(1251)));
            }
            catch(FileNotFoundException exp)
            {
                MessageBox.Show("\nВ папке с программой нет шапки.");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(textBox1.Visible && comboBox2.SelectedIndex == 3 && dictoanary != null)
            {
                MessageBox.Show("\n Пожалуйста, загрузите для этого алгоритма набор даных, где ключи только числа");
                dictoanary = null;
                dataGridView1.Columns.Clear();
                dataGridView2.Columns.Clear();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns.Clear();
            dataGridView2.Columns.Clear();
            label2.Visible = false;
            using (OpenFileDialog openFileDialog1 = new OpenFileDialog())
            {
                openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog1.InitialDirectory = "E:\\c\\Алгоритмы и структуры данных";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    Program.ReadFile(ref dictoanary, openFileDialog1.FileName);
                    if( dictoanary == null)
                    {
                        return;
                    }
                    dataGridView1.Columns.Add("firstcolumn", "Ключ");
                    dataGridView1.Columns.Add("firstcolumn", "Значение");
                    
                    for (int k = 0; k < dictoanary.Length; k++)
                    {
                        dataGridView1.Rows.Add(new object[] { dictoanary[k].Key, dictoanary[k].Value });
                    }
                }
                    
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void помощьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(File.ReadAllText(@"Шапка.txt", Encoding.GetEncoding(1251)));
            }
            catch (FileNotFoundException exp)
            {
                MessageBox.Show("\nВ папке с программой нет шапки.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //вызывать меседж бокс с шапкой
        }

        private void сбежатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (dictoanary == null)
            {
                MessageBox.Show("Вы не открыли файл");
                return;
            }
            //Algoritmes.AlgoritmesShell(ref dictoanary,out CountSwap);
            dictoanaryCopy = new MyDict[dictoanary.Length];
            Array.Copy(dictoanary,dictoanaryCopy,dictoanary.Length);
            Count = Program.SelectAlgoritmSort(comboBox2,ref dictoanaryCopy);
            if (Count == -1 || comboBox2.SelectedIndex == -1)
            {
                //MessageBox.Show("Количество перестановок неправильное");
                return;
            }
            else
            {
                dataGridView2.Columns.Clear();
                dataGridView2.Columns.Add("firstcolumn", "Ключ");
                dataGridView2.Columns.Add("firstcolumn", "Значение");
                label2.Text = "Количество перестановок =" + Count.ToString();
                
                label2.Visible = true;
                for (int k = 0; k < dictoanaryCopy.Length; k++)
                {
                    dataGridView2.Rows.Add(new object[] { dictoanaryCopy[k].Key, dictoanaryCopy[k].Value });

                }
                
                chart1.Series["Кол-во перестановок"].Points.AddXY(comboBox2.Text, Count);
                Count = 0;
                //button1.Visible = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.Visible = true;
            button4.Visible = false;
            textBox1.Visible = false;
            //dataGridView1.Columns.Clear();
            dataGridView2.Columns.Clear();
            //dictoanary = null;
            label3.Visible = false;
            label2.Visible = false;
            comboBox2.Items.Clear();
            comboBox2.Items.Add("Сортировка Шелла");
            comboBox2.Items.Add("Сортировка простым выбором");
            comboBox2.Items.Add("Пирамидальная сортировка");
            comboBox2.Items.Add("Сортировка слиянием");
            comboBox2.Items.Add("Пузырьковая сортировка");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            button4.Visible = true;
            textBox1.Visible = true;
            //dataGridView1.Columns.Clear();
            dataGridView2.Columns.Clear();
            //dictoanary = null;
            label3.Visible = true;
            label2.Visible = false;
            textBox1.Clear();
            comboBox2.Items.Clear();
            comboBox2.Items.Add("Бинарный поиск");
            comboBox2.Items.Add("Линейный поиск");
            comboBox2.Items.Add("Линейный упорядоченый поиск");
            comboBox2.Items.Add("Интерполяционый поиск");
            comboBox2.Items.Add("Поиск в бинарном дереве");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dictoanary == null)
            {
                MessageBox.Show("\nВы не открыли файл");
                return;
            }
            if (textBox1.Text == "")
            {
                MessageBox.Show("Вы не ввели ключ");
                return;

            }

            if (comboBox2.SelectedIndex == -1)
            {
                MessageBox.Show("\nВы не выбрали алгоритм");
                return;
            }
            else
            {
                if (comboBox2.SelectedIndex == 3)
                {
                    //MessageBox.Show("\nДля данного алгоритма нужно адаптировать ключи(убрать из ключей нецифры).\n Программа сделает это сама с уже считанными данными \n Если в ключе нет цифр,то он станет 99999 \n Ключ тоже должен быть в виде цифр \n В этом случае ключи 99 и 099 считаются одинаковыми ");
                    double x;
                    if (!double.TryParse(textBox1.Text,out x))
                    {
                        MessageBox.Show("\nКлюч  должен быть в виде цифр для данного алгоритма");
                        return;
                    }
                    //Program.Adapt(ref dictoanary);
                    dataGridView1.Columns.Clear();
                    dataGridView1.Columns.Add("firstcolumn", "Ключ");
                    dataGridView1.Columns.Add("firstcolumn", "Значение");
                    for (int kk = 0; kk < dictoanary.Length; kk++)
                    {
                        dataGridView1.Rows.Add(new object[] { dictoanary[kk].Key, dictoanary[kk].Value });
                    }
                   
                    
                }
                dataGridView2.Columns.Clear();
                dataGridView2.Columns.Add("firstcolumn", "Ключ");
                dataGridView2.Columns.Add("firstcolumn", "Значение");
                dictoanaryCopy = new MyDict[dictoanary.Length];
                Array.Copy(dictoanary, dictoanaryCopy, dictoanary.Length);
                //int k = Program.SelectAlgoritmSearch(comboBox2,ref  dictoanaryCopy, textBox1,out Count);
                MyDict Result = Program.SelectAlgoritmSearch(comboBox2, ref dictoanaryCopy, textBox1, out Count);
                if (Result == null)
                {
                    MessageBox.Show("\nЭлемента с таким ключом нет");
                    return;
                }
                label2.Text = "Количество сравнений =" + Count.ToString();
                label2.Visible = true;
                dataGridView2.Rows.Add(new object[] {Result.Key, Result.Value });
                chart1.Series["Кол-во сравнений"].Points.AddXY(comboBox2.Text, Count);
                Count = 0;

            }
        }

        private void помощьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("\nДопустимые разделители { ';',':'} \n Запись в файле должна быть формата: \nКлюч;(:)Значение \nКлюч;(:)Значение \n");
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
