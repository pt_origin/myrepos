﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Алгоритмы_и_структуры_данных
{
    public class Tree
    {
        public string key;
        public string value1;
        private Tree left;
        private Tree right;


        public void Insert(string key,string value)
        {
            if (this.key == null)
            {
                this.key = key;
                this.value1 = value;
            }
            else
            {
                if (this.key.CompareTo(key) == 1)
                {
                    if (left == null)
                        this.left = new Tree();
                    left.Insert(key,value);
                }
                else if (this.key.CompareTo(key) == -1)
                {
                    if (right == null)
                        this.right = new Tree();
                    right.Insert(key,value);
                }
                else
                {
                    this.value1 = value;
                }
                    
                
            }
            
        }
        public Tree Search(string key,ref int count)
        {
            count = count + 2;
            if (this.key == key)
            {
                count--;
                return this;

            }
            
            else if (this.key.CompareTo(key) == 1)
            {
                if (left != null)
                    return this.left.Search(key, ref count);
                else
                    return null;
            }
            else
            {
                if (right != null)
                    return this.right.Search(key, ref count);
                else
                    return null;
            }
        }
        
        public void Clear()
        {
            this.key = null;
            this.left = null;
            this.right = null;
        }
    }
}