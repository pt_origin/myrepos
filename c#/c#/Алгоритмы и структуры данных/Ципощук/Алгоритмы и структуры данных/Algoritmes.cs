﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Алгоритмы_и_структуры_данных
{

    internal static class Algoritmes
    {
        static Algoritmes()
        {
            //throw new System.NotImplementedException();
        }
        static void heap(ref MyDict[] md, int n, int i, ref int CW)
        {
            int largest = i;
            int l = 2 * i + 1;
            int r = 2 * i + 2;

            if (l < n &&  md[l].Key.CompareTo(md[largest].Key) > 0 )
                largest = l;

            if (r < n &&  md[r].Key.CompareTo(md[largest].Key) > 0)
                largest = r;

            if (largest != i)
            {
                
                MyDict.Swap(ref md[i],ref md[largest]);
                CW++;
                heap(ref md, n, largest,ref CW);
            }
        }

        public static void AlgoritmesPiramidalnaya(ref MyDict[] md, out int CW)
        {

            CW = 0;
            for (int i = md.Length / 2 - 1; i >= 0; i--)
                heap(ref md, md.Length, i, ref CW);

            for (int i = md.Length - 1; i >= 0; i--)
            {
                
                MyDict.Swap(ref md[0],ref md[i]);
                CW++;
                heap(ref md, i, 0, ref CW);

            }
        }
        public static void AlgoritmesBubbleSort(ref MyDict[] md,out int CountSwap)
        {
            CountSwap = 0;
            for (int i = 0; i < md.Length; i++)
            {
                for (int j = 0; j < md.Length - 1 - i; j++)
                {
                    if (md[j].Key.CompareTo(md[j + 1].Key) > 0)
                    {
                        MyDict.Swap(ref md[j], ref md[j + 1]);
                        CountSwap++;
                    }
                }


            }
        }

        static MyDict[] Merge_Sort(MyDict[] massive,ref int cw)
        {
            if (massive.Length == 1)
                return massive;
            int mid_point = massive.Length / 2;
            return Merge(Merge_Sort(massive.Take(mid_point).ToArray(),ref cw), Merge_Sort(massive.Skip(mid_point).ToArray(),ref cw),ref cw);
        }
        static MyDict[] Merge(MyDict[] mass1, MyDict[] mass2,ref int cw)
        {
            int a = 0, b = 0;
            MyDict[] merged = new MyDict[mass1.Length + mass2.Length];
            for (int i = 0; i < mass1.Length + mass2.Length; i++)
            {
                if (b.CompareTo(mass2.Length) < 0 && a.CompareTo(mass1.Length) < 0)
                {

                    if (mass1[a].Key.CompareTo(mass2[b].Key) > 0)
                    {
                        merged[i] = mass2[b++];
                        cw++;
                    }

                    else
                    {
                        merged[i] = mass1[a++];
                        cw++;
                    }

                }
                        
                else
                {
                    if (b < mass2.Length)
                    {
                        merged[i] = mass2[b++];
                        cw++;
                    }
                        
                    else
                    {
                        merged[i] = mass1[a++];
                        cw++;
                    }
                        

                }
                    
            }
            return merged;
        } 
        public static void TechSort(MyDict [] md)
        {
            for (int i = 0; i < md.Length; i++)
            {
                int min = i;
                for (int j = i ; j < md.Length; j++)
                {
                    if (Convert.ToInt32( md[j].Key) < Convert.ToInt32(md[min].Key) )
                    {
                        min = j;
                    }
                }
                MyDict.Swap(ref md[i], ref md[min]);
                

            }

        }
        public static int AlgoritmesOrderedLinearSearch(ref MyDict [] md,string key,out int Count)
        {
            Count = 0;
            for(int i = 0; i < md.Length; i++)
            {
                if (md[i].Key.CompareTo(key) <= 0)
                {
                    Count++;
                    if (md[i].Key.Equals(key))
                    {
                        Count++;
                        return i;

                    }
                        

                }
                    
            }
            return -1;
        }

        public static void AlgoritmesMerge(ref MyDict [] md,out int CountSwap)
        {
            CountSwap = 0;
            md = Merge_Sort(md,ref CountSwap);
        }
        public static int AlgoritmesBinarySearch(ref MyDict[]md, string key,out int CountCompare)
        {
            CountCompare = 0;
            if ((md.Length == 0) ||  key.CompareTo(md[0].Key) < 0 || key.CompareTo(md[md.Length - 1].Key) > 0)
                return -1;
            int first = 0;
            int last = md.Length;
            while (first < last)
            {
                int mid = first + (last - first) / 2;
                CountCompare++;
                if (key.CompareTo(md[mid].Key) <= 0)
                    last = mid;
                else
                    first = mid + 1;
            }
            CountCompare++;
            if (md[last].Key.Equals(key))
                return last;
            else
                return -1;
        }
        public static int AlgoritmesInterpolationSearch(ref MyDict [] md, int key, out int CountCompare)
        {
            CountCompare = 0;
            int mid;
            int left = 0;
            int right = md.Length - 1;
            while (Convert.ToInt32( md[left].Key) <= key && Convert.ToInt32(md[right].Key) >= key)
            {
                try
                {
                    mid = left + ((key - Convert.ToInt32(md[left].Key)) * (right - left)) / (Convert.ToInt32(md[right].Key) - Convert.ToInt32(md[left].Key));

                }
                catch
                {
                    MessageBox.Show("\n Скорее всего вы дали массив с одинаковым ключами");
                    return -1;
                }

               

                if (Convert.ToInt32(md[mid].Key) < key)
                {
                    CountCompare++;
                    left = mid + 1;
                }
                else if (Convert.ToInt32(md[mid].Key) > key)
                {
                    CountCompare+=2;
                    right = mid - 1;
                }
                else
                {
                    CountCompare +=2;
                    return mid;
                }
                
            }

            if (Convert.ToInt32(md[left].Key) == key)
            {
                CountCompare++;
                return left;
                
            }
            else if (Convert.ToInt32(md[right].Key) == key)
            {
                CountCompare+=2;
                return right;
            }
            else
            {
                return -1;
            }
        }
        public static int AlgoritmesLinearSearch(ref MyDict [] md, string key,out int CountCompare)
        {
            CountCompare = 0;
            for (int i = 0; i < md.Length; i++)
            {
                CountCompare++;
                
                if (md[i].Key.Equals(key))
                {
                    
                    return i;
                }
            }

            return -1;
        }
        public static void AlgoritmesShell(ref MyDict[] md, out int CountSwap)
        {
            int step = md.Length / 2, j;
            CountSwap = 0;
            MyDict temp = new MyDict();
            while(step > 0)
            {
                for (int i = 0; i < (md.Length - step); i++)
                {
                    j = i;

                    while ((j >= 0) && md[j].Key.CompareTo(md[j + step].Key) > 0 /*Convert.ToInt32( md[j].Key) > Convert.ToInt32(md[j + step].Key)*/)
                    {

                        MyDict.Swap(ref md[j], ref md[j + step]);
                        CountSwap++;
                        j -= step;
                    }

                }

                step = step / 2;
            }
           

        }

        public static void  AlgoritmesEasyChoose(ref MyDict[] md, out int CountSwap)
        {
            CountSwap = 0;
            for (int i = 0; i < md.Length; i++)
            {
                int min = i;
                for (int j = i + 1; j < md.Length; j++)
                {
                    if (md[j].Key.CompareTo(md[min].Key) < 0/*Convert.ToInt32(md[j].Key) < Convert.ToInt32(md[min].Key)*/)
                    {
                        min = j;
                    }
                }
                MyDict.Swap(ref md[i],ref md[min]);
                CountSwap++;
                
            }

        }
    }
    public class MyDict
    {
        private string key;
        private string myvalue;

        public MyDict()
        {
            Key = "";
            Value = "";
        }


        public static void Swap(ref MyDict md1,ref MyDict md2)
        {
            if (md2 == null || md1 == null)
                throw new Exception("\nOne argument is null");
            MyDict tmp = new MyDict(md1.Key, md1.Value);
            Copy(ref md1, md2);
            Copy(ref md2, tmp);
        }

        public MyDict(string key,string value)
        {
            Key = key;
            Value = value;
        }
        public string Key
        {
            get
            {
                return key;
            }
            set
            {
                key = value;
            }
        }

        public string Value
        {
            get
            {
                return myvalue;
            }
            set
            {
                myvalue = value;
            }
        }

        public static void Copy(ref MyDict to,MyDict from)
        {
            to = new MyDict(from.Key, from.Value);
        }
        public override bool Equals(object obj)
        {
            if (obj is MyDict)
                return this.ToString() == obj.ToString();
            else return false;
        }
        public bool Equals(MyDict md)
        {
            return this.ToString() == md.ToString();
        }

        public override string ToString()
        {
            return "Ключ = " + Key.ToString() + "Значение = " +  Value.ToString();
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}