﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text;

namespace Алгоритмы_и_структуры_данных
{
     public static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }


        //public static void Copy(ref MyDict[] dictSource, ref MyDict[] dict2To)
        //{
        //    if (dictSource == null)
        //        return;
        //    for (int i = 1 ; i <= dictSource.Length; i++)
        //    {
        //        try
        //        {
        //            Array.Resize<MyDict>(ref dict2To, i);
        //            dict2To[i] = new MyDict(dictSource[i - 1].Key, dictSource[i - 1].Value);

        //        }
        //        catch(Exception e)
        //        {
        //            MessageBox.Show(i.ToString());
        //        }
                

        //    }
        //}
        public static void ReadFile(ref MyDict[] myDict,string filename)
        {
            try
            {
                int i = 0;
                StreamReader file = new StreamReader(filename, Encoding.GetEncoding(1251));
                string line;
                char[] separators = { ';', ':' };
                string[] buf;
                while ((line = file.ReadLine()) != null)
                {
                    buf = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    try
                    {
                        if (buf.Length != 2)
                            
                            throw new InvalidDataException("\nОшибочный формат входных данных,в строчке не 2 разделителя,допустимые разделители { ';',':'} \n Запись в файле должна быть формата: \nКлюч;(:)Значение \nКлюч;(:)Значение");

                    }
                    catch (Exception e)
                    {
                        myDict = null;
                        MessageBox.Show(e.Message);
                        return;
                    }
                    i++;
                    Array.Resize<MyDict>(ref myDict, i);
                    myDict[i - 1] = new MyDict(buf[0], buf[1]);
                }
            }
            catch(FileNotFoundException e)
            {
                myDict = null;
                MessageBox.Show(e.Message + "Проверьте правильность имени файла");
                return;
            }
            catch (InvalidDataException e)
            {
                myDict = null;
                MessageBox.Show(e.Message);
                return;
            }
        }
        public static void Adapt(ref MyDict [] myDicts)
        {
            //string[] text = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u"," v","w","x", "y", "z" };

            for(int i = 0; i < myDicts.Length;i++)
            {
                string justNumbers = new String(myDicts[i].Key.Where(Char.IsDigit).ToArray());
                if(justNumbers == null || justNumbers == "")
                {
                    justNumbers = "99999";
                }
                myDicts[i].Key = justNumbers;
                //for (int j = 0; j < text.Length; j++)
                //{
                //myDicts[i].Key.Replace(text[j], "");
                //myDicts[i].Key.Replace(text[j].ToUpper(), "");
                //}
            }
        }
        public static MyDict SelectAlgoritmSearch(ComboBox box,ref MyDict[] dict,TextBox textBox1,out int CountCompare)
        {
            int position = -1;
            int rubb;
            CountCompare = 0 ;
            switch(box.SelectedIndex)
            {
                case 0:
                    Algoritmes.AlgoritmesMerge(ref dict, out rubb);
                    position = Algoritmes.AlgoritmesBinarySearch(ref dict, textBox1.Text, out CountCompare);
                    //Algoritmes.AlgoritmesShell(ref dict, out CountSwap);
                    break;
                case 1:
                    position = Algoritmes.AlgoritmesLinearSearch(ref dict, textBox1.Text, out CountCompare);
                    break;
                case 3:
                    //Algoritmes.AlgoritmesMerge(ref dict, out rubb);
                    Algoritmes.TechSort(dict);
                    position = Algoritmes.AlgoritmesInterpolationSearch(ref dict, Convert.ToInt32(textBox1.Text), out CountCompare);
                    
                    break;
                case 2:
                    Algoritmes.AlgoritmesMerge(ref dict, out rubb);
                    position = Algoritmes.AlgoritmesOrderedLinearSearch(ref dict, textBox1.Text,out CountCompare);
                    //Algoritmes.AlgoritmesMerge(ref dict, out CountSwap);
                    break;
                case 4:
                    Tree MyTree = new Tree();
                    for(int i = 0; i < dict.Length; i++)
                    {
                        MyTree.Insert(dict[i].Key, dict[i].Value);
                    }
                    try
                    {
                        Tree Result = MyTree.Search(textBox1.Text,ref CountCompare);
                        return (new MyDict(Result.key, Result.value1));
                    }
                    catch(NullReferenceException e)
                    {
                        return null;
                    }
                    
                    
                    //Algoritmes.AlgoritmesBubbleSort(ref dict,out CountSwap);
                    break;
                default:
                    //CountSwap = 0;
                    MessageBox.Show("\nВы выбрали не тот алгоритм,попробуйте еще");
                    break;
                    
            }
            if (position != -1)
                return dict[position];
            else return null;
        }

        public static int SelectAlgoritmSort(ComboBox box, ref MyDict[] dict)
        {
            int CountSwap = -1;
            switch (box.SelectedIndex)
            {
                case 0:
                    Algoritmes.AlgoritmesShell(ref dict, out CountSwap);
                    break;
                case 1:
                    Algoritmes.AlgoritmesEasyChoose(ref dict, out CountSwap);
                    break;
                case 2:
                    Algoritmes.AlgoritmesPiramidalnaya(ref dict, out CountSwap);
                    break;
                case 3:
                    Algoritmes.AlgoritmesMerge(ref dict, out CountSwap);
                    break;
                case 4:
                    Algoritmes.AlgoritmesBubbleSort(ref dict, out CountSwap);
                    break;
                default:
                    CountSwap = 0;
                    MessageBox.Show("\nВы не выбрали алгоритм,попробуйте еще");
                    break;

            }
            return CountSwap;
        }
    }
}
