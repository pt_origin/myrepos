﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_номер_1_вариант_2
{
    class Program
    {
        /*enum Moves
        {
            Up = 1,
            Down,
            Left,
            Right
        }*/
        /*static void Move(int latitude , int longitude ){


            }*/

        static void Main(string[] args)
        {
            Console.WriteLine(" Введите размерность поля");
           /* int intBuf;bool flag = true;
            while (flag)
            {
                string txt = Console.ReadLine();
                try
                {
                    intBuf = Convert.ToInt32(txt);
                    flag = false;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(" Это не число");
                    flag = true;
                }
            }*/int size;
            while (!Int32.TryParse(Console.ReadLine(), out size ))
            {
                Console.WriteLine("Это не подходит,попробуй еще. ");
            }
            //size = Convert.ToInt32(Console.ReadLine());
            char[,] arr_interface = new char[size, size];
            Random rnd = new Random();
            int latitude_hoard = rnd.Next(size-1); // first coord
            int longitude_hoard = rnd.Next(size); // second coord
            for (int i = 0; i < arr_interface.GetLength(0); i++)
            {
                for (int j = 0; j < arr_interface.GetLength(1); j++)
                {
                    arr_interface[i, j] = '*';
                }

            }
            int latitude_player = size - 1; // first coord player
            int longitude_player = 0; // second coord player
            int distanse = Math.Abs(latitude_player - latitude_hoard) + Math.Abs(longitude_player - longitude_hoard);

            do
            {
                distanse = Math.Abs(latitude_player - latitude_hoard) + Math.Abs(longitude_player - longitude_hoard);
                Console.WriteLine(" До клада осталось {0} шагов", distanse);
                for (int i = 0; i < arr_interface.GetLength(0); i++)
                {
                    for (int j = 0; j < arr_interface.GetLength(1); j++)
                    {
                        string player = "Ты";
                        if (i == latitude_player && j == longitude_player)
                        {
                            Console.Write("{0,4}", player);
                        }
                        else { Console.Write("{0,4}", arr_interface[i, j]); }
                    }
                    Console.WriteLine();

                }
                Console.WriteLine("Введите 1 , чтобы продвинуться вниз , 2 , чтобы продвинуться вверх, 3, чтобы продвинуться вправо , 4, чтобы продвинуться влево ");
                int move;
                //move = Convert.ToInt32(Console.ReadLine());
                while (!Int32.TryParse(Console.ReadLine(), out move))
                {
                    Console.WriteLine("Это не подходит,попробуй еще. ");
                }
                switch (move)
                {
                    case 1:
                        latitude_player++;
                        break;
                    case 2:
                        latitude_player--;
                        break;
                    case 3:
                        longitude_player++;
                        break;
                    case 4:
                        longitude_player--;
                        break;
                    default:
                        Console.WriteLine("Вы ввели не верное число,вы остаетесь на месте");
                        break;

                }
                if (latitude_player > size-1 || latitude_player < 0 || longitude_player > size-1 || longitude_player < 0)
                {
                    Console.WriteLine(" Вы попытались выйти за пределы поля и взорвались на мине. Игра окончена ");
                    break;
                }
                distanse = Math.Abs(latitude_player - latitude_hoard) + Math.Abs(longitude_player - longitude_hoard);
            } while (distanse != 0);
            if (distanse == 0)
            {
                Console.WriteLine("Урааа! Вы нашли клад ");
            }
            

            Console.ReadLine();
        }
    }
}