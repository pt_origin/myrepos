﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_номер__2
{
    public class Student
    {
        private Person Persons;
        private Education Educations;
        private int NumberGroups;
        private Exam [] _exams;

        public Person Person
        {
            get
            {
                //throw new System.NotImplementedException();
                return Persons;
            }
            set
            {
                Persons = value;
            }
        }

        public int NumberGroup
        {
            get
            {
                //throw new System.NotImplementedException();
                return NumberGroups;
            }
            set
            {
                NumberGroups = value;
            }
        }

        public Education Education
        {
            get
            {
                //throw new System.NotImplementedException();
                return Educations;
            }
            set
            {
                Educations = value;
            }
        }

        public Exam[] myExam
        {
            get
            {
                
                return _exams;
            }
            set
            {
               _exams = value;
            }
        }

        public double AverageMarks
        {
            get
            {
                int size = this._exams.Length;
                if (size == 0)
                {
                    Console.WriteLine("Quantity Exams = 0");
                    return 0;
                }
                int average = 0;
                for (int i = 0; i < size; i++)
                    average += this._exams[i].Mark;
                average = average/size;
                    return average;
            }
            
        }

        public Student()
        {
            Person = new Person();
            Education = 0;
            NumberGroup = 0;//Неизвестных в общую нулевую?
            _exams = new Exam[3];
            for (int i = 0; i < _exams.Length; i++)
                _exams[i] = new Exam(90 + (i + 1), (i + 1) + "ый екзамен", DateTime.Today);
        }
        public Student( Person Persons , Education Educations , int NumberGroups)
        {
            this.NumberGroup = NumberGroups;
            this.Person = Persons;
            this.Education = Educations;
            _exams = new Exam[0];
           // for (int i = 0; i < _exams.Length; i++)
                //_exams[i] = new Exam(90 + i + 1, (i+1) + "ый екзамен", DateTime.Today);
        }
        public void AddExams(params Exam[] exams)
        {
            int iLength = _exams.Length;
            Array.Resize(ref _exams, _exams.Length +  exams.Length);
            for (int i = 0; i < exams.Length;i ++)
                _exams[i + iLength] = exams[i];

        }
        public override string ToString()
        {
            string s = "";
            s = s + string.Format("Student:{0} \nEducations: {1} \nNumber group: {2} \n",this.Person.ToString(),this.Education,this.NumberGroup);
            foreach (var st in _exams)
                s = s + st.ToString();
            return s ;

        }
        public string ToShortString()
        {
            string s = "";
            s = s + string.Format("Student:{0} \nEducations: {1} \nNumber group: {2} \nAverage Marks = {3}\n", this.Person.ToString(), this.Education, this.NumberGroup,this.AverageMarks);
            return s;
        }
    }
}
