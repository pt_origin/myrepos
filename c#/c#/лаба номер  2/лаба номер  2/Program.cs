﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace лаба_номер__2
{
    public enum Education
    {
        Specialist = 0,
        Вachelor = 1,
        SecondEducation = 2
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student MyStudent = new Student();
            //Student MyStudent2 = new Student(new Person("IIIII", "SSSSSS", new DateTime(2000, 1, 1)), Education.Вachelor, 2);
            //Console.WriteLine(MyStudent2.ToShortString());
            Console.WriteLine(MyStudent.ToShortString());
            MyStudent.Person = new Person("Pavel", "Tsiposchuk", new DateTime(2000, 5, 12));
            MyStudent.Education = Education.Вachelor;
            MyStudent.NumberGroup = 1;
            Console.WriteLine(MyStudent.ToString());
            Exam[] ex = {
                             new Exam(92,"Основы программирования",new DateTime(2018,6,18)),
                             new Exam(92,"Линейная Алгебра",new DateTime(2018,6,21)),
                             new Exam(90,"Матанализ", new DateTime(2018,6,25)),
                             new Exam(98,"Дискретная математика",new DateTime(2018,6,30))
                         };
            MyStudent.AddExams(ex);
            Console.WriteLine(MyStudent.ToString());
            Console.WriteLine("Input size array n rows and m cols, you can use this delims:',' '.' '/' '_' ' '");
            int[] size = Console.ReadLine().Split(new char[] { ',', '.', '/', '_',' ' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray<int>();
            int n = size[0];
            int m = size[1];
            Console.WriteLine("n = {0} , m = {1} ",n,m);
            Exam [] ArrExam1 = new Exam[n * m];
            for (int i = 0; i < ArrExam1.Length; i++)
                ArrExam1[i] = new Exam(5, "Math", new DateTime(2018, 6, 30));
            Exam[,] ArrExam2 = new Exam[n, m];
            for (int j = 0; j < ArrExam2.GetLength(0); j++)
                for (int i = 0; i < ArrExam2.GetLength(1); i++)
                    ArrExam2[j, i] = new Exam(5, "Math", new DateTime(2018, 6, 30));
            Exam [][] ArrExam3 = new Exam [n][];
            for (int i = 0; i < ArrExam3.Length; i++)
            {
                ArrExam3[i] = new Exam[m];
                for (int j = 0; j < ArrExam3[i].Length; j++)
                    ArrExam3[i][j] = new Exam(5, "Math", new DateTime(2018, 6, 30));
            }

            int time = Environment.TickCount;
            for (int i = 0; i < ArrExam1.Length; i++)
                ArrExam1[i].Mark = 2;
            int time1 = Environment.TickCount;
            Console.WriteLine("Для одномерного массива прошло " + Math.Abs((time - time1)));
            time = Environment.TickCount;
            for (int i = 0; i < ArrExam2.GetLength(0); i++)
                for (int j = 0; j < ArrExam2.GetLength(1); j++)
                    ArrExam2[i, j].Mark = 2;
            time1 = Environment.TickCount;
            Console.WriteLine("Для двумерного массива прошло " + Math.Abs((time - time1)));
            time = Environment.TickCount;
            for (int i = 0; i < ArrExam3.Length; i++)
                for (int j = 0; j < ArrExam3[i].Length; j++)
                    ArrExam3[i][j].Mark = 2;
            time1 = Environment.TickCount;
            Console.WriteLine("Для ступенчатого массива прошло " + Math.Abs((time - time1)));
            

            Console.ReadLine();

        }
    }
}
