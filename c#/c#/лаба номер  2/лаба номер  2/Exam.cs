﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_номер__2
{
    public class Exam
    {
        //private int p_mark;
        //private string p_name;
        //private System.DateTime p_date_time;
        public int Mark
        {
            get;
            set;
        }
        public string Name
        {
            get
            ;
            set
           ;
        }
        public System.DateTime Date
        {
            get;
            set;

        }
        public Exam()
        {
            this.Name = "Unknown";
            this.Mark = 2;
            this.Date = System.DateTime.Now;
        }
        public Exam(int p_mark, string p_name, System.DateTime date_time)
        {
            this.Name = p_name;
            this.Mark = p_mark;
            this.Date = date_time;
        }
        public Exam(int p_mark)
        {
            this.Mark = p_mark;
        }
        public Exam(System.DateTime p_date_time)
        {
            this.Date = p_date_time;
        }
        public Exam(string p_name)
        {
            this.Name = p_name;
        }
        public override string ToString()
        {
            return string.Format("Name of exam = " + this.Name + "\n" + "Mark = " + this.Mark + "\n" + "Date Exam = " + this.Date + "\n");
            
        }


    }
}

