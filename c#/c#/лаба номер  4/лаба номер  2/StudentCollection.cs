﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_номер__2
{
    public class StudentCollection
    {
        private System.Collections.Generic.List<Student> Group;
        public StudentCollection()
        {
            Group = new List<Student> ();
        }
        public void AddDefaults()
        {
            Group.Add(new Student());
            Group.Add(new Student(new Person("StudentName","StudentSurName",new DateTime(2000,1,1)),0,1));
            Group.Add(new Student(new Person("StudentName2", "StudentSurName2", new DateTime(2000, 2, 2)), 0, 1));
        
        }
        public void AddStudents(params Student[] group)
        {
            for (int i = 0; i < group.Length; i++)
            {
                Group.Add(group[i]);
            }
        }

        public override string ToString()
        {
            string str;
            str = "Group\n";
            for (int i = 0; i < Group.Count; i++)
            {
                str += Group[i].ToString();
            }
            return str;
        }
        public  string ToShortString()
        {
            string str;
            str = "Group\n";
            for (int i = 0; i < Group.Count; i++)
            {
                str += Group[i].ToShortString();
            }
            return str;
        }
        public void SortBySurName()
        {
            Group.Sort();  
        }
        public void SortByBirthday()
        {
            Group.Sort(new Person());
        }
        public void SortByMarks()
        {
            Group.Sort(new HelpClass());
        }



    }
}
