﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace w1234
{
    [Serializable]
    public class Person :  IComparable, IComparer<Person>
    {
        protected string firstname;
        protected string surname;
        protected System.DateTime birthday;

        public string FirstName
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
            }
        }
        public string SurName
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }
        public DateTime Birthday
        {
            get
            {
                return birthday;
            }
            set
            {
                birthday = value;
            }
        }
        public int YearBirth
        {
            get
            {
                return birthday.Year;
            }
            set
            {
                birthday = new DateTime(value, birthday.Month, birthday.Day);
            }
        }
        public Person(string name, string surname, DateTime birthday)
        {
            FirstName = name;
            SurName = surname;
            Birthday = birthday;
        }
        public Person()
        {
            FirstName = "Unknown FirstName";
            SurName = "Unknown SurName";
            Birthday = DateTime.Today;
        }
        public override string ToString()
        {

            return "Name of person= " + this.FirstName + "\n" + "Surname = " + this.SurName + "\n" + "Birthday = " + this.Birthday;

        }
        public virtual string ToShortString()
        {
            return string.Format("Name of person= " + this.FirstName + "\n" + "Surname = " + this.SurName + "\n");

        }
        public override bool Equals(object obj)
        {
            if (obj is Person)
            {
                Person temp = obj as Person;
                return this.ToString() == temp.ToString();
            }
            else return false;
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        public static bool operator ==(Person p1, Person p2)
        {
            return p1.Equals(p2);
        }
        public static bool operator !=(Person p1, Person p2)
        {
            return !p1.Equals(p2);
        }
        //public Person DeepCopy()
        //{
        //    return new Person(this.FirstName, this.SurName, this.Birthday);
        //}
        int IComparable.CompareTo(object obj1)
        {
            Person temp = obj1 as Person;
            if (temp != null)
            {
                return this.SurName.CompareTo(temp.SurName);
            }
            else throw new ArgumentException("Param is not person");
            
        }
        int IComparer<Person>.Compare(Person obj1,Person obj2)
        {
            Person temp1 = obj1 as Person;
            Person temp2 = obj2 as Person;
            if (temp1 != null && temp2 != null)
            {
                return temp1.Birthday.CompareTo(temp2.Birthday);
            }
            else throw new ArgumentException("Param not Person");
            //return 0;
        }
    }
    
}
