﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace w1234
{
    public enum Education
    {
        Specialist = 0,
        Вachelor = 1,
        SecondEducation = 2
    }
    [Serializable]
    class Student : Person
    {
        public Person Person
        {
            get
            {
                return new Person(this.FirstName, this.SurName, this.Birthday);
            }
            set
            {
                this.FirstName = value.FirstName;
                this.SurName = value.SurName;
                this.Birthday = value.Birthday;
            }
        }

        public Education Education { get; set; }

        private int NumberGroups;

        public int NumberGroup
        {
            get
            {
                return NumberGroups;
            }
            set
            {
                if (value > 100 || value < 1)
                {
                    throw new System.InvalidOperationException("Границы значения группы от 1 до 100 ");
                }
                else
                {
                    NumberGroups = value;
                }
            }
        }

        public List<Exam> MyExam { get; set; }


        public double AverageMarks
        {
            get
            {
                double size = this.MyExam.Count;
                if (size == 0)
                {
                    Console.WriteLine("Quantity Exams = 0");
                    return 0;
                }
                double average = 0;
                for (int i = 0; i < size; i++)
                {
                    average += this.MyExam[i].Mark;
                }


                average = average / size;
                return average;
            }

        }

        public Student()
        {

            Education = 0;
            NumberGroup = 99;//Неизвестных в общую нулевую?
            MyExam = new List<Exam>();
            
        }


        public Student(Person Persons, Education Educations, int NumberGroups) : base(Persons.FirstName, Persons.SurName, Persons.Birthday)
        {
            this.NumberGroup = NumberGroups;
            this.Education = Educations;
            MyExam = new List<Exam>();
        }

        public void AddExams(params Exam[] exams)
        {
            
            for (int i = 0; i < exams.Length; i++)
                MyExam.Add(exams[i]);

        }

        public override string ToString()
        {
            string s = "";
            s = s + string.Format("Student:{0} \nEducations: {1} \nNumber group: {2} \n", this.Person.ToString(), this.Education, this.NumberGroup);
            for (int i = 0; i < MyExam.Count; i++)
                s = s + MyExam[i].ToString();
           
            return s;

        }

        public override string ToShortString()
        {
            string s = "";
            s = s + string.Format("Student:{0} \nEducations: {1} \nNumber group: {2} \nAverage Marks = {3}\n", this.Person.ToString(), this.Education, this.NumberGroup, this.AverageMarks);
            return s;
        }

        public override bool Equals(object obj)
        {
            if (obj is Student)
            {
                Student temp = obj as Student;
                return this.ToString() == temp.ToString();
            }
            else return false;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public static bool operator ==(Student p1, Student p2)
        {
            return p1.Equals(p2);
        }

        public static bool operator !=(Student p1, Student p2)
        {
            return !p1.Equals(p2);
        }

        public IEnumerable<Exam> GetExamIterator(int mark)
        {
            int max = MyExam.Count;
            for (int i = 0; i < max; i++)
            {
                if (MyExam[i].Mark > mark)
                {
                    yield return MyExam[i];
                }
            }
        }

        public Student DeepCopy2()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Student));
            using (MemoryStream stream = new MemoryStream())
            {

                xmlSerializer.Serialize(stream, this);
                stream.Position = 0;
                return xmlSerializer.Deserialize(stream) as Student;
            }

        }

        public Student DeepCopy()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter xmlSerializer = new BinaryFormatter();
                xmlSerializer.Serialize(stream, this);
                stream.Position = 0;
                return xmlSerializer.Deserialize(stream) as Student;
            }

        }

        public void Initialization(Student st)
        {
            if (st == null)
                throw new ArgumentNullException();
            this.Person = st.Person;
            this.Birthday = st.Birthday;
            this.Education = st.Education;
            this.MyExam = st.MyExam;
            this.NumberGroup = st.NumberGroup;
            //this.
        }

        public bool Save(string filename)
        {
            using (FileStream stream = new FileStream(filename, FileMode.Create,FileAccess.Write))
            {
                try
                {
                    //XmlSerializer xmlSerializer = new XmlSerializer(typeof(Student));
                    BinaryFormatter xmlSerializer = new BinaryFormatter();
                    xmlSerializer.Serialize(stream, this);
                    return true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                    Console.ReadLine();
                    return false;
                }
                
            }            
        }

        public bool Load(string filename)
        {
            Student buf = this.DeepCopy();
            try
            {
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    try
                    {
                        //XmlSerializer xmlSerializer = new XmlSerializer(typeof(Student));
                        BinaryFormatter xmlSerializer = new BinaryFormatter();
                        this.Initialization(xmlSerializer.Deserialize(stream) as Student);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        this.Initialization(buf);
                        Console.ReadLine();

                        return false;

                    }

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            
        }

        static public bool Save(string filename, Student obj)
        {
            using (FileStream stream = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                try
                {
                    //XmlSerializer xmlSerializer = new XmlSerializer(typeof(Student));
                    BinaryFormatter xmlSerializer = new BinaryFormatter();
                    xmlSerializer.Serialize(stream,obj);
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Console.ReadLine();

                    return false;
                }

            }
        }

        static public bool Load(string filename, Student obj)
        {
            Student buf = obj.DeepCopy();
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                try
                {
                    //XmlSerializer xmlSerializer = new XmlSerializer(typeof(Student));
                    BinaryFormatter xmlSerializer = new BinaryFormatter();
                    obj.Initialization(xmlSerializer.Deserialize(stream) as Student);
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    obj.Initialization(buf);
                    Console.ReadLine();

                    return false;

                }

            }
        }

        public bool AddFromConsole()
        {
            Console.WriteLine("Введите оценку,название и дату экзамена в формате : \n Оценка;Название предмета;Год:месяц:день");
            try
            {
                char[] separ = { ';' };
                string[] buf = Console.ReadLine().Split(separ , StringSplitOptions.RemoveEmptyEntries);
                if (buf.Length != 3)
                    throw new Exception("Too small");
                Exam newEx = new Exam
                {
                    Mark = Convert.ToInt32(buf[0]),
                    Name = buf[1]
                };
                separ[0] = ':';
                string [] bufdate = buf[2].Split(separ, StringSplitOptions.RemoveEmptyEntries);
                if (bufdate.Length != 3)
                    throw new Exception("Wrong date");
                newEx.DateExam = new DateTime(Convert.ToInt32(bufdate[0]), Convert.ToInt32(bufdate[1]), Convert.ToInt32(bufdate[2]));
                this.AddExams(newEx);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                return false;
            }
            return true;
        }


    }
}
