﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;


namespace w1234
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st = new Student(new Person("Vasya", "Efremov", DateTime.Now), Education.Specialist, 12);
            st.AddExams(new Exam(), new Exam(78, "Predmet", new DateTime(2000, 12, 12)));
            Student st2 = st.DeepCopy2();
            Console.WriteLine(st.ToString());
            Console.WriteLine(st2.ToString());


            st.Save("1.txt");


            Student st3 = new Student();
            bool flag = false;
            Console.WriteLine("Filename input pls");
            string file = Console.ReadLine();
            if(File.Exists(file))
            {
                if (st3.Load(file))
                    flag = true;
                else return;
            }
            else
            {
                Console.WriteLine("File doesn't exist");
                FileStream fs =  File.Create(file);
                fs.Close();
            }

            //Console.WriteLine(file.ToString());

            if(flag)
            {
                Console.WriteLine(st3.ToString());
                st3.AddFromConsole();
                if (!st3.Save("12.txt"))
                {
                    //Console.WriteLine("2e1");
                    return;
                   

                }
                    
                Console.WriteLine(st3.ToString());
                if (!Student.Load(file, st3))
                    return;

                st3.AddFromConsole();
                if (!Student.Save(file, st3))
                    return;
                Console.WriteLine(st3);
            }
            

                

            Console.ReadLine();
        }
    }
}
