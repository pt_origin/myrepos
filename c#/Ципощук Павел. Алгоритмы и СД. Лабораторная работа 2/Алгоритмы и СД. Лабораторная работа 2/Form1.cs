﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Алгоритмы_и_СД.Лабораторная_работа_2
{
    public partial class Form1 : Form
    {

        UnlimQueue<string> FirstQueue = new UnlimQueue<string>();
        UnlimQueue<string> SecondQueue = new UnlimQueue<string>();
        public Form1()
        {
            InitializeComponent();
        }

        private void OpenFIle_Click(object sender, EventArgs e)
        {
            UnlimQueue<string> test = new UnlimQueue<string>();
            using (OpenFileDialog openFileDialog1 = new OpenFileDialog())
            {
                openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    Program.ReadFile(ref test , openFileDialog1.FileName);
                    if (test == null)
                    {
                        return;
                    }
                    else
                    {
                        FirstQueue = test;
                        ViewFirstUpdate();
                    }

                }

            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void AddFirstQueue_Click(object sender, EventArgs e)
        {
            if (FirstQueueAddTextBox.Text == "")
                return;
            FirstQueue.Enqueue(FirstQueueAddTextBox.Text);
            ViewFirstUpdate();

        }

        private void ViewFirstUpdate()
        {
            
            ViewFirst.Rows.Clear();
            int i = 1;
            foreach (var st in FirstQueue)
                ViewFirst.Rows.Add(new object[] { i++,st.ToString() });
            label5.Text = "Is Empty = " + FirstQueue.IsEmpty;
            label6.Text = "Size = " + FirstQueue.Size();
            //MessageBox.Show(FirstQueue.Size().ToString());

        }

        private void ViewSecondUpdate()
        {
            
            ViewSecond.Rows.Clear();
            int i = 1;
            foreach (var st in SecondQueue)
                ViewSecond.Rows.Add(new object[] { i++, st.ToString() });
            label7.Text = "Is Empty = " + SecondQueue.IsEmpty;
            label8.Text = "Size = " + SecondQueue.Size();
            //MessageBox.Show(FirstQueue.Size().ToString());

        }

        private void EqueueButton_Click(object sender, EventArgs e)
        {
            if(FirstQueue.IsEmpty)
            {
                MessageBox.Show("\nОчередь пуста", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show(FirstQueue.Dequeue(), "Dequeue", MessageBoxButtons.OK, MessageBoxIcon.Information) ;
            ViewFirstUpdate();
        }

        private void PeekFirstQueue_Click(object sender, EventArgs e)
        {
            if (FirstQueue.IsEmpty)
            {
                MessageBox.Show("\nОчередь пуста", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            MessageBox.Show(FirstQueue.Peek(), "Peek", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //ViewFirstUpdate();
        }

        private void AddSecondQueue_Click(object sender, EventArgs e)
        {
            if (SecondQueueAddTextBox.Text == "")
                return;
            SecondQueue.Enqueue(SecondQueueAddTextBox.Text);
            ViewSecondUpdate();
        }

        private void SecondEqueueButton_Click(object sender, EventArgs e)
        {
            if (SecondQueue.IsEmpty)
            {
                MessageBox.Show("\nОчередь пуста", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show(SecondQueue.Dequeue(), "Dequeue", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ViewSecondUpdate();
        }

        private void SecondPeekQueue_Click(object sender, EventArgs e)
        {
            if (SecondQueue.IsEmpty)
            {
                MessageBox.Show("\nОчередь пуста", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show(SecondQueue.Peek(), "Peek", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void AggregateButton_Click(object sender, EventArgs e)
        {
            if(FirstQueue.IsEmpty || SecondQueue.IsEmpty)
            {
                MessageBox.Show("\nОдна из очередей пуста", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FirstQueue = UnlimQueue<string>.Aggregate(FirstQueue, SecondQueue);
            ViewFirstUpdate();
        }

        private void DivideButton_Click(object sender, EventArgs e)
        {
            if (DivideBox.Text == "")
            {
                MessageBox.Show("\nВведите длину очереди,которую хотите отделить от первой", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int x;
            if (!int.TryParse(DivideBox.Text, out x))
            {
                MessageBox.Show("\nКлюч  должен быть в виде цифр для данного алгоритма", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (FirstQueue.IsEmpty || FirstQueue.Size() < 2)
            {
                MessageBox.Show("Первая очередь слишком мала,чтобы поделить ее", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (FirstQueue.Size() < Convert.ToInt32(DivideBox.Text))
            {
                MessageBox.Show("\nПервая очередь слишком мала,чтобы отделить от нее очередь длиной в  " + x.ToString() + " экземпляров", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SecondQueue= UnlimQueue<string>.Divide(ref FirstQueue, x);
            ViewSecondUpdate();
            ViewFirstUpdate();
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            if(FirstQueue.IsEmpty)
            {
                MessageBox.Show("\nПервая очередь пуста,копировать нечего","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SecondQueue = FirstQueue.Copy();
            ViewSecondUpdate();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void AggregateButton2_Click(object sender, EventArgs e)
        {
            if (FirstQueue.IsEmpty || SecondQueue.IsEmpty)
            {
                MessageBox.Show("\nОдна из очередей пуста", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SecondQueue = UnlimQueue<string>.Aggregate(FirstQueue, SecondQueue);
            ViewSecondUpdate();
            ViewFirstUpdate();
        }

        private void DivideButton2_Click(object sender, EventArgs e)
        {
            if (DivideBox2.Text == "")
            {
                MessageBox.Show("\nВведите длину очереди,которую хотите отделить от первой", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int x;
            if (!int.TryParse(DivideBox2.Text, out x))
            {
                MessageBox.Show("\nКлюч  должен быть в виде цифр для данного алгоритма", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (SecondQueue.IsEmpty || SecondQueue.Size() < 2)
            {
                MessageBox.Show("Первая очередь слишком мала,чтобы поделить ее", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (SecondQueue.Size() < Convert.ToInt32(DivideBox2.Text))
            {
                MessageBox.Show("\nПервая очередь слишком мала,чтобы отделить от нее очередь длиной в  " + x.ToString() + " экземпляров", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FirstQueue = UnlimQueue<string>.Divide(ref SecondQueue, x);
            ViewSecondUpdate();
            ViewFirstUpdate();
        }

        private void CopyButton2_Click(object sender, EventArgs e)
        {
            if (SecondQueue.IsEmpty)
            {
                MessageBox.Show("\nВторая очередь пуста,копировать нечего", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FirstQueue = SecondQueue.Copy();
            ViewSecondUpdate();
            ViewFirstUpdate();

        }

        private void DeleteFirstButton_Click(object sender, EventArgs e)
        {
            FirstQueue.Delete();
            ViewFirstUpdate();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void DeleteSecondButton_Click(object sender, EventArgs e)
        {
            SecondQueue.Delete();
            ViewSecondUpdate();
        }
    }
}
