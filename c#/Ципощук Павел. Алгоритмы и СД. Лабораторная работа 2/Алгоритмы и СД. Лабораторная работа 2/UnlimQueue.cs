﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;

namespace Алгоритмы_и_СД.Лабораторная_работа_2
{
    internal class ListQ<T>
    {
        public T Object { get; set; }
        public ListQ<T> Next;
        public ListQ<T> Prev;
        public ListQ(T obj,ListQ<T> next,ListQ<T> prev)
        {
            Object = obj;
            Next = next;
            Prev = prev;
            
        }
        public ListQ(T obj)
        {
            Object = obj;
            Next = null;
            Prev = null;
        }
        public override string ToString()
        {
            return Object.ToString();
        }
    }
    internal class UnlimQueue<T> : IEnumerable<T>
    {
        private ListQ<T> Head;
        private ListQ<T> Tail;
        private bool isEmpty = true;
        public bool IsEmpty { get => isEmpty; set => isEmpty = value; }
        public UnlimQueue()
        {
            Head = null;
            Tail = null;
        }
        public UnlimQueue(T Tobj)
        {
            ListQ<T> obj = new ListQ<T>(Tobj);
            if (IsEmpty)
            {
                Head = obj;
                Tail = obj;
                IsEmpty = true;
            }
            else throw new Exception();

        }
        private void Add(ListQ<T> obj)//Добавляет независимую копию передаваемого объекта
        {
            ListQ<T> obj2 = new ListQ<T>(obj.Object);
            if(IsEmpty)
            {
                Head = obj2;
                Head.Next = obj2;
                Tail.Prev = null;
                Tail = obj2;
                Tail.Prev = obj2;
                Tail.Next = null;
                IsEmpty = false;
            }
            else
            {
                Tail.Next = obj2;
                obj2.Prev = Tail;
                Tail = obj2;
                Tail.Next = null;
                
            }

        }
        static public UnlimQueue<T> Divide(ref UnlimQueue<T> dividend,int count )
        {
            if (count > dividend.Size())
            {
                return null;
            }
            else if (count == dividend.Size())
                return new UnlimQueue<T>();

            UnlimQueue<T> first1 = new UnlimQueue<T>();
            //UnlimQueue<T> second = new UnlimQueue<T>(); Если хочу забрать каунт человек с конца очереди,то она надо

            for(int i = 0;i < count;i++)
            {
                first1.Enqueue(dividend.Dequeue());
            }
            return first1;
            

        }
        public void Enqueue(T Tobj)
        {
            ListQ<T> obj = new ListQ<T>(Tobj);
            if (IsEmpty)
            {
                Head = obj;
                Head.Next = obj;
                Tail = obj;
                Tail.Prev = null;
                
                Tail.Prev = obj;
                Tail.Next = null;
                IsEmpty = false;
            }
            else
            {
                Tail.Next = obj;
                obj.Prev = Tail;
                Tail = obj;
                Tail.Next = null;

            }

        }
		public T Dequeue()
		{
            if (IsEmpty)
                return default(T);
			//T res = new T();
			T res = DeepClone(Head.Object);
            if(Head == Tail)
            {
                isEmpty = true;
                Head = null;
                Tail = null;
                return res;
            }

			Head = Head.Next;
			Head.Prev = null;
			return res;
		}    
		//private static T CreateCopy<T>(T aobject)
		//{
		//	MethodInfo memberwiseClone = aobject.GetType().GetMethod("MemberwiseClone", BindingFlags.Instance | BindingFlags.NonPublic);
		//	T Copy = (T)memberwiseClone.Invoke(aobject, null);
		//	foreach (FieldInfo f in typeof(T).GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
		//	{
		//		object original = f.GetValue(aobject);
		//		f.SetValue(Copy, CreateCopy(original));
		//	}
		//	return Copy;
		//}// Problems
        static T DeepClone<T>(T aobject)//Good
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, aobject);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }
        public int Size()
        {

            if (isEmpty)
                return 0;
            int res = 1;
            ListQ<T> Current = Head;
            while(!Current.Equals(Tail))
            {
                res++;
                Current = Current.Next;

            }
            return res;

        }
        private ListQ<T> peek ()
        {
            return Head;
        }
        public T Peek()
        {
            return Head.Object;
        }
        public void Delete()
        {
            Head = null;
            Tail = null;
            IsEmpty = true;
        }
        public bool IsEqual(UnlimQueue<T> Q)
        {
            return Object.ReferenceEquals(this, Q);

        }
        //public bool IsSimiliar(UnlimQueue<T> Q)
        //{
        //    if (this.IsEqual(Q))
        //        return true;
        //    if (this.Size() != Q.Size() || this.IsEmpty != this.IsEmpty)
        //        return false;
        //    ListQ<T> currentThis = Head;
        //    ListQ<T> currentQ = Q.Head;
        //    while ((currentThis != null && currentQ != null) || currentQ)
        //    {
        //        //currentThis.Object;
        //        currentThis = currentThis.Next;
        //    }

        //}

        public UnlimQueue<T> Copy ()//Возвращает независимую копию очереди
        {
            UnlimQueue<T> New = new UnlimQueue<T>();
            if (this.IsEmpty)
                return null;
            ListQ<T> Current = Head;
            if (this.Size() == 1)
            {
                New.Enqueue(DeepClone<T>(Current.Object));
                return New;
            }
            while (!Current.Equals(Tail))
            {
                New.Enqueue(DeepClone<T>(Current.Object));
                Current = Current.Next;
            }
            New.Enqueue(DeepClone<T>(Current.Object));
            return New;


        }
        static public UnlimQueue<T> Aggregate(UnlimQueue<T> First,UnlimQueue<T> Second)//возвращает независимое объединение очередей
        {
            if (First.IsEmpty)
            {
                if (Second.IsEmpty)
                {
                    return null;
                }
                else return Second;

            }
            else if (Second.IsEmpty)
                return First;
            
            UnlimQueue<T> NewFirst = First.Copy();
            UnlimQueue<T> NewSecond = Second.Copy();
            NewFirst.Tail.Next = NewSecond.Head;
            NewFirst.Tail.Next.Prev = NewFirst.Tail;
            NewFirst.Tail = NewSecond.Tail;
            return NewFirst;

        }
        public override string ToString()
        {
            string str = "";
            if(this == null)
            {
                return "Очередь пуста";
            }
            ListQ<T> Current = Head;
            if (this.Size() == 1)
            {
                return Current.Object.ToString();
            }
            while (!Current.Equals(Tail))
            {
                str = str + Current.Object.ToString();
                Current = Current.Next;
            }
            
            return str;
        }
        public IEnumerator<T> GetEnumerator()
        {
            ListQ<T> current = Head;
            while (current != null)
            {
                yield return current.Object;
                current = current.Next;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
    }
}