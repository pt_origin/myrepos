﻿namespace Алгоритмы_и_СД.Лабораторная_работа_2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.OpenFIle = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ViewFirst = new System.Windows.Forms.DataGridView();
            this.Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Elem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DeleteFirstButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PeekFirstQueue = new System.Windows.Forms.Button();
            this.EqueueButton = new System.Windows.Forms.Button();
            this.AddFirstQueue = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.FirstQueueAddTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.DeleteSecondButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SecondPeekQueue = new System.Windows.Forms.Button();
            this.SecondEqueueButton = new System.Windows.Forms.Button();
            this.AddSecondQueue = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SecondQueueAddTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ViewSecond = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AggregateButton = new System.Windows.Forms.Button();
            this.DivideButton = new System.Windows.Forms.Button();
            this.DivideBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CopyButton = new System.Windows.Forms.Button();
            this.AggregateButton2 = new System.Windows.Forms.Button();
            this.DivideButton2 = new System.Windows.Forms.Button();
            this.DivideBox2 = new System.Windows.Forms.TextBox();
            this.CopyButton2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ViewFirst)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ViewSecond)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenFIle});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // OpenFIle
            // 
            this.OpenFIle.Name = "OpenFIle";
            this.OpenFIle.Size = new System.Drawing.Size(98, 20);
            this.OpenFIle.Text = "Открыть файл";
            this.OpenFIle.Click += new System.EventHandler(this.OpenFIle_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ViewFirst
            // 
            this.ViewFirst.AllowUserToAddRows = false;
            this.ViewFirst.AllowUserToDeleteRows = false;
            this.ViewFirst.AllowUserToResizeColumns = false;
            this.ViewFirst.AllowUserToResizeRows = false;
            this.ViewFirst.BackgroundColor = System.Drawing.Color.White;
            this.ViewFirst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ViewFirst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Num,
            this.Elem});
            this.ViewFirst.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ViewFirst.Location = new System.Drawing.Point(60, 16);
            this.ViewFirst.MultiSelect = false;
            this.ViewFirst.Name = "ViewFirst";
            this.ViewFirst.ReadOnly = true;
            this.ViewFirst.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ViewFirst.ShowEditingIcon = false;
            this.ViewFirst.Size = new System.Drawing.Size(192, 350);
            this.ViewFirst.TabIndex = 1;
            this.ViewFirst.TabStop = false;
            // 
            // Num
            // 
            this.Num.Frozen = true;
            this.Num.HeaderText = "№";
            this.Num.Name = "Num";
            this.Num.ReadOnly = true;
            this.Num.Width = 50;
            // 
            // Elem
            // 
            this.Elem.Frozen = true;
            this.Elem.HeaderText = "Элемент";
            this.Elem.Name = "Elem";
            this.Elem.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DeleteFirstButton);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.PeekFirstQueue);
            this.panel1.Controls.Add(this.EqueueButton);
            this.panel1.Controls.Add(this.AddFirstQueue);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.FirstQueueAddTextBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ViewFirst);
            this.panel1.Location = new System.Drawing.Point(47, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(314, 510);
            this.panel1.TabIndex = 2;
            // 
            // DeleteFirstButton
            // 
            this.DeleteFirstButton.Location = new System.Drawing.Point(60, 469);
            this.DeleteFirstButton.Name = "DeleteFirstButton";
            this.DeleteFirstButton.Size = new System.Drawing.Size(192, 23);
            this.DeleteFirstButton.TabIndex = 10;
            this.DeleteFirstButton.Text = "Удалить первую очередь";
            this.DeleteFirstButton.UseVisualStyleBackColor = true;
            this.DeleteFirstButton.Click += new System.EventHandler(this.DeleteFirstButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(206, 495);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "             ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(57, 495);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "             ";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // PeekFirstQueue
            // 
            this.PeekFirstQueue.Location = new System.Drawing.Point(60, 440);
            this.PeekFirstQueue.Name = "PeekFirstQueue";
            this.PeekFirstQueue.Size = new System.Drawing.Size(192, 23);
            this.PeekFirstQueue.TabIndex = 7;
            this.PeekFirstQueue.Text = "Показать из очереди";
            this.PeekFirstQueue.UseVisualStyleBackColor = true;
            this.PeekFirstQueue.Click += new System.EventHandler(this.PeekFirstQueue_Click);
            // 
            // EqueueButton
            // 
            this.EqueueButton.Location = new System.Drawing.Point(60, 411);
            this.EqueueButton.Name = "EqueueButton";
            this.EqueueButton.Size = new System.Drawing.Size(192, 23);
            this.EqueueButton.TabIndex = 6;
            this.EqueueButton.Text = "Извлечь из очереди";
            this.EqueueButton.UseVisualStyleBackColor = true;
            this.EqueueButton.Click += new System.EventHandler(this.EqueueButton_Click);
            // 
            // AddFirstQueue
            // 
            this.AddFirstQueue.Location = new System.Drawing.Point(60, 383);
            this.AddFirstQueue.Name = "AddFirstQueue";
            this.AddFirstQueue.Size = new System.Drawing.Size(67, 23);
            this.AddFirstQueue.TabIndex = 5;
            this.AddFirstQueue.Text = "Добавить";
            this.AddFirstQueue.UseVisualStyleBackColor = true;
            this.AddFirstQueue.Click += new System.EventHandler(this.AddFirstQueue_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 369);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Добавить элемент";
            // 
            // FirstQueueAddTextBox
            // 
            this.FirstQueueAddTextBox.Location = new System.Drawing.Point(152, 385);
            this.FirstQueueAddTextBox.Name = "FirstQueueAddTextBox";
            this.FirstQueueAddTextBox.Size = new System.Drawing.Size(100, 20);
            this.FirstQueueAddTextBox.TabIndex = 3;
            this.FirstQueueAddTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(106, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Первая очередь";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.DeleteSecondButton);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.SecondPeekQueue);
            this.panel2.Controls.Add(this.SecondEqueueButton);
            this.panel2.Controls.Add(this.AddSecondQueue);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.SecondQueueAddTextBox);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ViewSecond);
            this.panel2.Location = new System.Drawing.Point(474, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(314, 510);
            this.panel2.TabIndex = 8;
            // 
            // DeleteSecondButton
            // 
            this.DeleteSecondButton.Location = new System.Drawing.Point(60, 469);
            this.DeleteSecondButton.Name = "DeleteSecondButton";
            this.DeleteSecondButton.Size = new System.Drawing.Size(192, 23);
            this.DeleteSecondButton.TabIndex = 11;
            this.DeleteSecondButton.Text = "Удалить вторую очередь";
            this.DeleteSecondButton.UseVisualStyleBackColor = true;
            this.DeleteSecondButton.Click += new System.EventHandler(this.DeleteSecondButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(203, 495);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "              ";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(57, 495);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "          ";
            // 
            // SecondPeekQueue
            // 
            this.SecondPeekQueue.Location = new System.Drawing.Point(60, 440);
            this.SecondPeekQueue.Name = "SecondPeekQueue";
            this.SecondPeekQueue.Size = new System.Drawing.Size(192, 23);
            this.SecondPeekQueue.TabIndex = 7;
            this.SecondPeekQueue.Text = "Показать из очереди";
            this.SecondPeekQueue.UseVisualStyleBackColor = true;
            this.SecondPeekQueue.Click += new System.EventHandler(this.SecondPeekQueue_Click);
            // 
            // SecondEqueueButton
            // 
            this.SecondEqueueButton.Location = new System.Drawing.Point(60, 411);
            this.SecondEqueueButton.Name = "SecondEqueueButton";
            this.SecondEqueueButton.Size = new System.Drawing.Size(192, 23);
            this.SecondEqueueButton.TabIndex = 6;
            this.SecondEqueueButton.Text = "Извлечь из очереди";
            this.SecondEqueueButton.UseVisualStyleBackColor = true;
            this.SecondEqueueButton.Click += new System.EventHandler(this.SecondEqueueButton_Click);
            // 
            // AddSecondQueue
            // 
            this.AddSecondQueue.Location = new System.Drawing.Point(60, 383);
            this.AddSecondQueue.Name = "AddSecondQueue";
            this.AddSecondQueue.Size = new System.Drawing.Size(67, 23);
            this.AddSecondQueue.TabIndex = 5;
            this.AddSecondQueue.Text = "Добавить";
            this.AddSecondQueue.UseVisualStyleBackColor = true;
            this.AddSecondQueue.Click += new System.EventHandler(this.AddSecondQueue_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 369);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Добавить элемент";
            // 
            // SecondQueueAddTextBox
            // 
            this.SecondQueueAddTextBox.Location = new System.Drawing.Point(152, 385);
            this.SecondQueueAddTextBox.Name = "SecondQueueAddTextBox";
            this.SecondQueueAddTextBox.Size = new System.Drawing.Size(100, 20);
            this.SecondQueueAddTextBox.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(106, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Вторая очередь";
            // 
            // ViewSecond
            // 
            this.ViewSecond.AllowUserToAddRows = false;
            this.ViewSecond.AllowUserToDeleteRows = false;
            this.ViewSecond.AllowUserToResizeColumns = false;
            this.ViewSecond.AllowUserToResizeRows = false;
            this.ViewSecond.BackgroundColor = System.Drawing.Color.White;
            this.ViewSecond.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ViewSecond.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.ViewSecond.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ViewSecond.Location = new System.Drawing.Point(60, 16);
            this.ViewSecond.MultiSelect = false;
            this.ViewSecond.Name = "ViewSecond";
            this.ViewSecond.ReadOnly = true;
            this.ViewSecond.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ViewSecond.ShowEditingIcon = false;
            this.ViewSecond.Size = new System.Drawing.Size(192, 350);
            this.ViewSecond.TabIndex = 1;
            this.ViewSecond.TabStop = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "Элемент";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // AggregateButton
            // 
            this.AggregateButton.Location = new System.Drawing.Point(368, 82);
            this.AggregateButton.Name = "AggregateButton";
            this.AggregateButton.Size = new System.Drawing.Size(100, 61);
            this.AggregateButton.TabIndex = 9;
            this.AggregateButton.Text = "Объединить в первую очередь";
            this.AggregateButton.UseVisualStyleBackColor = true;
            this.AggregateButton.Click += new System.EventHandler(this.AggregateButton_Click);
            // 
            // DivideButton
            // 
            this.DivideButton.Location = new System.Drawing.Point(368, 149);
            this.DivideButton.Name = "DivideButton";
            this.DivideButton.Size = new System.Drawing.Size(64, 48);
            this.DivideButton.TabIndex = 10;
            this.DivideButton.Text = "Поделить первую очередь ";
            this.DivideButton.UseVisualStyleBackColor = true;
            this.DivideButton.Click += new System.EventHandler(this.DivideButton_Click);
            // 
            // DivideBox
            // 
            this.DivideBox.Location = new System.Drawing.Point(437, 164);
            this.DivideBox.Name = "DivideBox";
            this.DivideBox.Size = new System.Drawing.Size(31, 20);
            this.DivideBox.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(393, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Операции";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // CopyButton
            // 
            this.CopyButton.Location = new System.Drawing.Point(367, 203);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(100, 45);
            this.CopyButton.TabIndex = 13;
            this.CopyButton.Text = "Копировать с первой очереди";
            this.CopyButton.UseVisualStyleBackColor = true;
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // AggregateButton2
            // 
            this.AggregateButton2.Location = new System.Drawing.Point(367, 254);
            this.AggregateButton2.Name = "AggregateButton2";
            this.AggregateButton2.Size = new System.Drawing.Size(100, 61);
            this.AggregateButton2.TabIndex = 14;
            this.AggregateButton2.Text = "Объединить во вторую очередь";
            this.AggregateButton2.UseVisualStyleBackColor = true;
            this.AggregateButton2.Click += new System.EventHandler(this.AggregateButton2_Click);
            // 
            // DivideButton2
            // 
            this.DivideButton2.Location = new System.Drawing.Point(368, 321);
            this.DivideButton2.Name = "DivideButton2";
            this.DivideButton2.Size = new System.Drawing.Size(64, 48);
            this.DivideButton2.TabIndex = 15;
            this.DivideButton2.Text = "Поделить вторую очередь ";
            this.DivideButton2.UseVisualStyleBackColor = true;
            this.DivideButton2.Click += new System.EventHandler(this.DivideButton2_Click);
            // 
            // DivideBox2
            // 
            this.DivideBox2.Location = new System.Drawing.Point(436, 336);
            this.DivideBox2.Name = "DivideBox2";
            this.DivideBox2.Size = new System.Drawing.Size(31, 20);
            this.DivideBox2.TabIndex = 16;
            // 
            // CopyButton2
            // 
            this.CopyButton2.Location = new System.Drawing.Point(367, 375);
            this.CopyButton2.Name = "CopyButton2";
            this.CopyButton2.Size = new System.Drawing.Size(100, 45);
            this.CopyButton2.TabIndex = 17;
            this.CopyButton2.Text = "Копировать со второй очереди";
            this.CopyButton2.UseVisualStyleBackColor = true;
            this.CopyButton2.Click += new System.EventHandler(this.CopyButton2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 561);
            this.Controls.Add(this.CopyButton2);
            this.Controls.Add(this.DivideBox2);
            this.Controls.Add(this.DivideButton2);
            this.Controls.Add(this.AggregateButton2);
            this.Controls.Add(this.CopyButton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.DivideBox);
            this.Controls.Add(this.DivideButton);
            this.Controls.Add(this.AggregateButton);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Алгоритмы и структуры данных. Лабораторная работа 2. Релиазация неограниченной оч" +
    "ереди на базе списка";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ViewFirst)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ViewSecond)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem OpenFIle;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView ViewFirst;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox FirstQueueAddTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AddFirstQueue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn Elem;
        private System.Windows.Forms.Button EqueueButton;
        private System.Windows.Forms.Button PeekFirstQueue;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button SecondPeekQueue;
        private System.Windows.Forms.Button SecondEqueueButton;
        private System.Windows.Forms.Button AddSecondQueue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SecondQueueAddTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView ViewSecond;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button AggregateButton;
        private System.Windows.Forms.Button DivideButton;
        private System.Windows.Forms.TextBox DivideBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.Button AggregateButton2;
        private System.Windows.Forms.Button DivideButton2;
        private System.Windows.Forms.TextBox DivideBox2;
        private System.Windows.Forms.Button CopyButton2;
        private System.Windows.Forms.Button DeleteFirstButton;
        private System.Windows.Forms.Button DeleteSecondButton;
    }
}

