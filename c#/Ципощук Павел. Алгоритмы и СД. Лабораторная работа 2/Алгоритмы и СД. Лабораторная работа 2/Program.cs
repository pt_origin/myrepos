﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Алгоритмы_и_СД.Лабораторная_работа_2
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public static void ReadFile(ref UnlimQueue<string> MyQ,string path)
        {
            try
            {
                using (StreamReader streamReader = new StreamReader(path, System.Text.Encoding.GetEncoding(1251)))
                {
                    
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        MyQ.Enqueue(line);



                    }
                }

            }
            catch (FileNotFoundException e)
            {
                MyQ = null;
                MessageBox.Show(e.Message + "Проверьте правильность имени файла");
                return;
            }
            catch (InvalidDataException e)
            {
                MyQ = null;
                MessageBox.Show(e.Message);
                return;
            }

        }
    }
}
