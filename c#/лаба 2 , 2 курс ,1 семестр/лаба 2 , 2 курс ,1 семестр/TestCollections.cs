﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public class TestCollections
    {
        private System.Collections.Generic.List<Person> Peoples;
        private System.Collections.Generic.List<string> Strings; 
        private System.Collections.Generic.Dictionary<Person, Student> Dict1;
        private System.Collections.Generic.Dictionary<string, Student> Dict2;


        public static Student AutoGeneration(int i)
        {
            Person p = new Person("Name" + i, "Surname" + i, new DateTime(2005, i % 12 + 1, i % 28 + 1));
            Student st = new Student();
            st.Person = p;
            return st;
        }
        public TestCollections()
        {
            Peoples = new List<Person>();
            Strings = new List<string>();
            Dict1 = new Dictionary<Person, Student>();
            Dict2 = new Dictionary<string, Student>();
        }
        public TestCollections(int size)
        {
            Peoples = new List<Person>();
            Strings = new List<string>();
            Dict1 = new Dictionary<Person, Student>();
            Dict2 = new Dictionary<string, Student>();
            for (int i = 0; i < size; i++)
            {
                Peoples.Add(new Person("Name" + i,"Surname" + i,new DateTime(2005,i%12+1,i%28+1)));
                Person p = new Person("Name" + i, "Surname" + i, new DateTime(2005, i % 12 + 1, i % 28 + 1));
                string st = p.ToString();
                Strings.Add(st);
                Person ptemp = new Person("Name" + i, "Surname" + i, DateTime.Today);
                Student stemp = AutoGeneration(i);
                Dict1.Add(p, stemp);
                Dict2.Add(st, stemp);
            }
        }

        public string Time(int i,Person p,Student st)
        {
            
            string info = "";
            bool temp;
            System.Diagnostics.Stopwatch time = new System.Diagnostics.Stopwatch();
            time.Start();
            
            temp = Peoples.Contains(p);
            time.Stop();
            info += String.Format("List peoples contains element : {1} \nElement searching time in List<Person> - {0}\n", time.Elapsed,temp);

            time.Reset();
            time.Start();
            string temp2;
            if(i < Strings.Count)
             temp2 = Strings[i];
            else temp2 = "YYYYYYYYYYYYYYY";
            temp = Strings.Contains(temp2);
            time.Stop();
            info += String.Format("List strings contains element : {1} \nElement searching time in List<People> - {0}\n", time.Elapsed, temp);

            time.Reset();
            time.Start();
            temp = Dict1.ContainsKey(p);
            time.Stop();
            info += String.Format("Dictionary<Person,Student> contains this key:{1} \nElement searching time by key in Dictionary<Person,Student> - {0}\n", time.Elapsed,temp);

            time.Reset();
            time.Start();
            temp = Dict1.ContainsValue(st);
            time.Stop();
            info += String.Format("Dictionary<Person,Student> contains this value:{1} \n Element searching time by value in Dictionary<Person,Student> - {0}\n", time.Elapsed,temp);

            time.Reset();
            time.Start();
            temp = Dict2.ContainsKey(temp2);
            time.Stop();
            info += String.Format("Dictoanary<string,Student> contains this key: {1} \n Element searching time by key in Dictionary<string,Student> - {0}\n", time.Elapsed,temp);

            time.Reset();
            time.Start();
            temp = Dict2.ContainsValue(st);
            time.Stop();
            info += String.Format("Dictoanary<string,Student> contains this value: {1} \n Element searching time by value in Dictionary<string,Student> - {0}\n", time.Elapsed,temp);

            return info;
            

        }
    }
}
