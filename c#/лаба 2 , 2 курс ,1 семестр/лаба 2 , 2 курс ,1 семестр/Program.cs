﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace лаба_2___2_курс__1_семестр
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentCollection<string> SC1 = new StudentCollection<string>("My_SC1111");
            StudentCollection<string> SC2 = new StudentCollection<string>("My_SC2222");
            Journal journal = new Journal();
            SC1.StudentsChanged += journal.StudentsCountChangedEventHandler;
            SC2.StudentsChanged += journal.StudentsCountChangedEventHandler;
            Student st1 = new Student();
            Exam[] ex = {
                             new Exam(30,"Основы программирования",new DateTime(2018,6,18)),
                             new Exam(30,"Линейная Алгебра",new DateTime(2018,6,21)),
                             new Exam(30,"Матанализ", new DateTime(2018,6,25)),
                             new Exam(30,"Дискретная математика",new DateTime(2018,6,30))
                         };
            st1.AddExams(ex);
            SC1.AddToDict("Ключ11", st1);
            Student st2 = new Student();

            st2.AddExams(new Exam(90, "Основы проadsdграммирования", new DateTime(2018, 6, 18)));
            st2.AddExams(new Exam(80, "Линейнаяasdas Алгебра", new DateTime(2018, 6, 21)));
            st2.AddExams(new Exam(80, "Матанаsaлиз", new DateTime(2018, 6, 25)));
            st2.AddExams(new Exam(90, "Дискрdasетная математика", new DateTime(2018, 6, 30)));
            SC1.AddToDict("Ключ12", st2);
            Student st3 = new Student();
            Exam[] ex3 = {
                             new Exam(10,"Основы программирования",new DateTime(2018,6,18)),
                             new Exam(10,"Линейная Алгебра",new DateTime(2018,6,21)),
                             new Exam(10,"Матанализ", new DateTime(2018,6,25)),
                             new Exam(18,"Дискретная математика",new DateTime(2018,6,30))
                         };
            st3.AddExams(ex3);
            SC1.AddToDict("Ключ13", st3);
            SC2.AddToDict("Ключ21", new Student(new Person("StudentName", "StudentSurName", new DateTime(2000, 1, 1)), 0, 1));
            SC2.AddToDict("Ключ22", new Student(new Person("StudentName2", "StudentSurName2", new DateTime(2300, 2, 2)), 0, 1));
            SC2.AddToDict("Ключ23", new Student(new Person("StudentName2333", "StudentSurName333332", new DateTime(2000, 2, 2)), 0, 1));
            SC1["Ключ11"].myExam = new List<Exam> {
                             new Exam(60, "Основы программирования", new DateTime(2018, 6, 18)),
                             new Exam(60, "Линейная Алгебра", new DateTime(2018, 6, 21)),
                             new Exam(60, "Матанализ", new DateTime(2018, 6, 25)),
                             new Exam(60, "Дискретная математика", new DateTime(2018, 6, 30))
                         };
            SC1["Ключ12"].myTest =  new List<Test> {
                            new Test("История", true),
                            new Test(),
                            new Test("English", true)
                        };
            SC2["Ключ21"].Education = Education.Вachelor;
            SC2["Ключ22"].Person = new Person("Поменяли Имя", "Мііі", DateTime.Today);
            SC2["Ключ23"].NumberGroup = 2;
            SC1.Remove(st3);
            st3.Person = new Person();
            st3.NumberGroup = 23;
            Console.WriteLine(journal.ToString());
            Console.WriteLine("Среднее значение коллекции " + SC1.Average().ToString());
            Console.ReadLine();

        }
    }
}
