﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public class JournalEntry
    {
        public string NameCollection { get; set; }
        public Action GetAction { get; set; }
        public string GetSender { get; set; }
        public string GetKey { get; set; }



        public JournalEntry(string namecollection, Action action, string sender, string key)
        {
            NameCollection = namecollection;
            GetAction = action;
            GetSender = sender;
            GetKey = key;
        }
        public override string ToString()
        {
            string str = "";
            str = "\n Событие произошло в " + NameCollection.ToString();
            str = str + "\n Тип события: " + GetAction.ToString() + "\nПричина :" + GetSender.ToString() + "\nКлюч :" + GetKey.ToString();
            return str;
        }
    }
}