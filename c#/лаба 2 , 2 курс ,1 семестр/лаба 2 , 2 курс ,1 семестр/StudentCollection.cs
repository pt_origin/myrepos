﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public class StudentCollection <Tkey>
    {
        private System.Collections.Generic.List<Student> Group;
        private Dictionary<Tkey, Student> Dict;
        public string NameCollection { get; set; }

        //public delegate void StudentListHandler(object source, StudentListHandlerEventArgs args);
        public delegate void StudentsChangedHandler<TKey>(object source,StudentsChangedEventArgs<TKey> args);
        public event StudentsChangedHandler<Tkey> StudentsChanged;
        public bool Remove(Student st)
        {
            if (Dict.ContainsValue(st))
            {
                foreach (var st1 in Dict.Where(s => s.Value.Equals(st)).ToList())
                {
                    Dict.Remove(st1.Key);
                    st1.Value.PropertyChanged -= PropSt;
                    StudentsChanged?.Invoke(this, new StudentsChangedEventArgs<Tkey>(NameCollection, Action.Remove, "", st1.Key));
                    
                }

            }
            else return false;
            //StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Удален элемент из колекции", Group[j]));
            

            return true;
        }
        public double Average()
        {
            return Dict.Average(s => s.Value.AverageMarks);
        }
        public Student this[Tkey key]
        {
            get
            {
                if (!Dict.ContainsKey(key))
                {
                    Console.WriteLine("Нет студента с таким ключом");
                    return null;
                }
                return Dict[key];
            }
            set
            {
                if(!Dict.ContainsKey(key))
                {
                    Console.WriteLine("Нет студента с таким ключом");
                    return;
                }
                //StudentReferenceChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Изменен элемент в колекцию", value));
                Dict[key] = value;
                //StudentsChanged?.Invoke(this, new StudentsChangedEventArgs<Tkey>(NameCollection, Action.Remove, "",key));


            }

        }

        //public event StudentListHandler StudentsCountChanged;
        //public event StudentListHandler StudentReferenceChanged;

        public StudentCollection(string namecollection)
        {
            NameCollection = namecollection;
            Dict = new Dictionary<Tkey,Student>();
        }
        //public void AddDefaults()
        //{
        //    Student NewSt = new Student();
        //    Group.Add(NewSt);
        //    //StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", NewSt));
        //    //StudentsCountChanged(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", NewSt));
        //    NewSt = new Student(new Person("StudentName", "StudentSurName", new DateTime(2000, 1, 1)), 0, 1);
        //    Group.Add(NewSt);
        //    //StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", NewSt));
        //    NewSt = new Student(new Person("StudentName2", "StudentSurName2", new DateTime(2000, 2, 2)), 0, 1);
        //    Group.Add(NewSt);
        //    //StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", NewSt));


        //}
        //public void AddStudents(params Student[] group)
        //{
        //    for (int i = 0; i < group.Length; i++)
        //    {
        //        Group.Add(group[i]);
        //        //StudentsCountChanged?.Invoke(this, new StudentListHandlerEventArgs(this.NameCollection, "Добавлен элемент в колекцию", group[i]));
        //    }
        //}
        public void AddToDict(Tkey key,Student st)
        {
            Dict.Add(key, st);
            StudentsChanged?.Invoke(this, new StudentsChangedEventArgs<Tkey>(NameCollection, Action.Add, "", key));
            st.PropertyChanged += PropSt;
        }
        private void PropSt(object sender, PropertyChangedEventArgs e)
        {
            Tkey item = Dict.FirstOrDefault(kvp => kvp.Value.Equals(sender)).Key;
            StudentsChanged?.Invoke(this, new StudentsChangedEventArgs<Tkey>(NameCollection, Action.Propety, e.PropertyName, item ));
                    
        }
        public override string ToString()
        {
            string str;
            str = "Group\n";
            for (int i = 0; i < Group.Count; i++)
            {
                str += Group[i].ToString();
            }
            return str;
        }
        //public  string ToShortString()
        //{
        //    string str;
        //    str = "Group\n";
        //    for (int i = 0; i < Group.Count; i++)
        //    {
        //        str += Group[i].ToShortString();
        //    }
        //    return str;
        //}
        //public void SortBySurName()
        //{
        //    Group.Sort();  
        //}
        //public void SortByBirthday()
        //{
        //    Group.Sort(new Person());
        //}
        //public void SortByMarks()
        //{
        //    Group.Sort(new HelpClass());
        //}



    }
}
