﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace лаба_2___2_курс__1_семестр
{
    public enum Education
    {
        Specialist = 0,
        Вachelor = 1,
        SecondEducation = 2
    }
    public enum Action
    {
        Add = 0,
        Remove = 1,
        Propety = 2
    }
    public class Student : Person,IDateAndCopy, System.ComponentModel.INotifyPropertyChanged
    {
        //private Person Persons;
        private Education Educations;
        private int NumberGroups;
        private System.Collections.Generic.List<Test> Tests;
        private System.Collections.Generic.List<Exam> Exams;

        public event PropertyChangedEventHandler PropertyChanged;

        public Person Person
        {
            get
            {
                //throw new System.NotImplementedException();
                return new Person(this.FirstName,this.SurName,this.Birthday);
            }
            set
            {
                this.FirstName = value.FirstName;
                this.SurName = value.SurName;
                this.Birthday = value.Birthday;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Person"));

            }
        }

        public int NumberGroup
        {
            get
            {
                //throw new System.NotImplementedException();
                return NumberGroups;
            }
            set
            {
                if (value > 100 || value < 1)
                {
                    //NumberGroups = 99;
                    throw new System.InvalidOperationException("Границы значения группы от 1 до 100 ");
                    
                }
                else
                {
                    NumberGroups = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NumberGroup"));
                }
            }
        }

        public Education Education
        {
            get
            {
                //throw new System.NotImplementedException();
                return Educations;
            }
            set
            {
                Educations = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Education"));
            }
        }

        public System.Collections.Generic.List<Exam> myExam
        {
            get
            {

                return Exams;
            }
            set
            {
               Exams = value;
               PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("myExam"));
            }
        }
        public System.Collections.Generic.List<Test> myTest //создать его вроде не требовалось в условии лр,но я создал на всяк случай
        {
            get
            {

                return Tests;
            }
            set
            {
                Tests = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Tests"));
            }
        }
        public double AverageMarks
        {
            get
            {
                double size = this.Exams.Count;
                if (size == 0)
                {
                    Console.WriteLine("Quantity Exams = 0");
                    return 0;
                }
                double average = 0;
                for (int i = 0; i < size; i++)
                {
                    average += this.Exams[i].Mark;
                    //Console.WriteLine(average.ToString());
                }
                    

                average = average/size;
                    return average;
            }
            
        }

        public Student()
        {
            
            Education = 0;
            NumberGroup = 99;//Неизвестных в общую нулевую?
            Exams = new List<Exam>();
            //for (int i = 0; i < 3; i++)
            //    Exams.Add(new Exam(90 + (i + 1), (i + 1) + "ый екзамен", DateTime.Today));
            Tests = new List<Test>();
            //for (int i = 0; i < 3; i++)
            //    Tests.Add(new Test((i + 1) + "ый зачет", true));
        }
        public Student( Person Persons , Education Educations , int NumberGroups) : base(Persons.FirstName,Persons.SurName,Persons.Birthday)
        {
            this.NumberGroup = NumberGroups;
            //this.Person = Persons;
            this.Education = Educations;
            Exams = new List<Exam>();
            Tests = new List<Test>();

          
        }
        public void AddExams(params Exam[] exams)
        {
            //int iLength = _exams.Length;
            //Array.Resize(ref _exams, _exams.Length +  exams.Length);
            for (int i = 0; i < exams.Length; i++)
                myExam.Add(exams[i]);

        }
        public void AddTests(params Test[] tests)
        {
            //int iLength = _exams.Length;
            //Array.Resize(ref _exams, _exams.Length +  exams.Length);
            for (int i = 0; i < tests.Length; i++)
                Tests.Add(tests[i]);

        }
        public override string ToString()
        {
            string s = "";
            s = s + string.Format("Student:{0} \nEducations: {1} \nNumber group: {2} \n",this.Person.ToString(),this.Education,this.NumberGroup);
            for(int i = 0;i < Exams.Count; i++)
                s = s + myExam[i].ToString();
            for (int i = 0; i < Tests.Count; i++)
                s = s + Tests[i].ToString();
            return s ;

        }
        public override string ToShortString()
        {
            string s = "";
            s = s + string.Format("Student:{0} \nEducations: {1} \nNumber group: {2} \nAverage Marks = {3}\n", this.Person.ToString(), this.Education, this.NumberGroup,this.AverageMarks);
            return s;
        }
        public override bool Equals(object obj)
        {
            if (obj is Student)
            {
                Student temp = obj as Student;
                return this.ToString() == temp.ToString();
            }
            else return false;
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        public static bool operator ==(Student p1, Student p2)
        {
            return p1.Equals(p2);
        }
        public static bool operator !=(Student p1, Student p2)
        {
            return !p1.Equals(p2);
        }
        public override object DeepCopy()
        {
            Student temp = new Student(new Person(this.FirstName,this.SurName,this.Birthday),this.Education,this.NumberGroup);
            temp.myExam = new List<Exam>();
            for (int i = 0; i < myExam.Count; i++)
                //temp.myExam.Add(new Exam(this.myExam[i].Mark,this.myExam[i].Name,this.myExam[i].DateExam));
                temp.myExam.Add((Exam)this.myExam[i].DeepCopy());
            temp.myTest = new List<Test>(this.myTest.Count);
            for (int i = 0; i < myTest.Count; i++)
                temp.myTest.Add(new Test(this.myTest[i].NameExam,this.myTest[i].PassExam));
            return temp;
        }
        public IEnumerable<Exam> GetExamIterator(int mark)
        {
            int max = myExam.Count;
            for (int i = 0; i < max; i++)
            {
                //if (i == myExam.Count)
                //{
                //    yield break;
                //}
                //else
                if(myExam[i].Mark > mark)
                {
                    yield return myExam[i];
                }
            }
        }
        public IEnumerable<object> GetAllIterator()
        {
            
            for (int i = 0; i < myExam.Count; i++)
            {
                yield return myExam[i];   
            }
            for (int i = 0; i < myTest.Count; i++)
            {
                yield return myTest[i];
            }
        }

       
       
        //public DateTime Date
        //{
        //    get
        //    {
        //        return this.Birthday;
        //    }
        //    set
        //    {
        //        this.Birthday = value;
        //    }
        //}
    }
}
