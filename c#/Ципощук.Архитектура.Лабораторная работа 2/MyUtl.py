﻿import sys
import argparse
import os
import shutil
import glob

def createParser ():
	parser = argparse.ArgumentParser()
	parser.add_argument ('-R', action = 'store_true', help = 'Включить рекурсивный подсчет по каталогу')
	parser.add_argument ('-D', nargs = 1, type = str, help = 'Установить директорию для подсчета файлов')
	parser.add_argument ('ext', nargs='*', help = 'Список масок')
	return parser

if __name__ == '__main__':
	parser = createParser()
	args = parser.parse_args(sys.argv[1:])
	
	if args.D:
		for dir in args.D:
			if os.path.exists(dir):
				path = dir
			else:
				print("Directory {} doesn't exist.".format(dir))
				exit(-1)
	else:
		path = os.path.dirname(os.path.abspath(__file__))
				
	if args.R:
		a = 0
		for rootdir, dirs, files in os.walk(path):
			for file in files:
				if len(args.ext) == 0:
					a = a + 1
				else:
					k = 0
					for i in args.ext:
						if i in file:
							k = 1
					if k == 1:
						a = a + 1
	else:
		a = 0
		for rootdir, dirs, files in os.walk(path):
			for file in files:
				if rootdir == path:
					if len(args.ext) == 0:
						a = a + 1
					else:
						k = 0
						for i in args.ext:
							if i in file:
								k = 1
						if k == 1:
							a = a + 1

	print("{}".format(a))
	exit(a)
