﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная_работа_номер_3_по_алгоритмам
{
    public class AVLNode
    {
        public int data;
        public AVLNode left;
        public AVLNode right;
        public AVLNode(int data)
        {
            this.data = data;
        }
    }
    class AVLTree
    {
        public bool IsEmpty()
        {
            if (root != null)
            {
                return false;
            }
            else return true;
        }
        private AVLNode root;
        public AVLNode Root
        {
            get
            {
                return root;
            }
        }
        public AVLTree()
        {


        }
        public void Add(int data)
        {
            AVLNode New = new AVLNode(data);
            if (root == null)
            {
                root = New;
            }
            else
            {
                root = Insert(root, New);
            }
        }
        private AVLNode Insert(AVLNode current, AVLNode InNod)
        {
            if (current == null)
            {
                current = InNod;
                return current;
            }
            else if (InNod.data < current.data)
            {
                current.left = Insert(current.left, InNod);
                current = Balance(current);
            }
            else if (InNod.data > current.data)
            {
                current.right = Insert(current.right, InNod);
                current = Balance(current);
            }
            return current;
        }
        private AVLNode Balance(AVLNode current)
        {
            int BFactor = BalanceFactor(current);
            if (BFactor > 1)
            {
                if (BalanceFactor(current.left) > 0)
                {
                    current = RotateLL(current);
                }
                else
                {
                    current = RotateLR(current);
                }
            }
            else if (BFactor < -1)
            {
                if (BalanceFactor(current.right) > 0)
                {
                    current = RotateRL(current);
                }
                else
                {
                    current = RotateRR(current);
                }
            }
            return current;
        }
        public void Delete(int target)
        {
            root = Delete(root, target);
        }
        public string Show(AVLNode elem)
        {
            if (elem == null)
                throw new ArgumentNullException();
            string s = "";
            s += "\nЗначение =" + elem.data.ToString();
            s += "\nВысота =" + GetHeight(elem).ToString();
            if (elem.left != null)
            s += "\nЛевый =" + elem.left.data.ToString();
            else s += "\nЛевого нет ";
            if (elem.right != null)
                s += "\nПравый =" + elem.right.data.ToString();
            else s += "\nПравого нет ";
            return s;

        }
        private AVLNode Delete(AVLNode current, int target)
        {
            AVLNode parent;
            if (current == null)
            { return null; }
            else
            {
                //left subtree
                if (target < current.data)
                {
                    current.left = Delete(current.left, target);
                    if (BalanceFactor(current) == -2)//here
                    {
                        if (BalanceFactor(current.right) <= 0)
                        {
                            current = RotateRR(current);
                        }
                        else
                        {
                            current = RotateRL(current);
                        }
                    }
                }
                //right subtree
                else if (target > current.data)
                {
                    current.right = Delete(current.right, target);
                    if (BalanceFactor(current) == 2)
                    {
                        if (BalanceFactor(current.left) >= 0)
                        {
                            current = RotateLL(current);
                        }
                        else
                        {
                            current = RotateLR(current);
                        }
                    }
                }
                //if target is found
                else
                {
                    if (current.right != null)
                    {
                        //delete its inorder successor
                        parent = current.right;
                        while (parent.left != null)
                        {
                            parent = parent.left;
                        }
                        current.data = parent.data;
                        current.right = Delete(current.right, parent.data);
                        if (BalanceFactor(current) == 2)//rebalancing
                        {
                            if (BalanceFactor(current.left) >= 0)
                            {
                                current = RotateLL(current);
                            }
                            else { current = RotateLR(current); }
                        }
                    }
                    else
                    {   //if current.left != null
                        return current.left;
                    }
                }
            }
            return current;
        }
        public AVLNode Find(int key)
        {
            AVLNode temp = null;
            try
            {
                if (Find(key, root, ref temp).data == key)
                {
                    if (temp != null)
                        return temp;
                    else return null;
                }
                else
                {
                    return null;
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        private AVLNode Find(int target, AVLNode current, ref AVLNode res)
        {
            if (current == null)
                throw new Exception("Нет такого элемента в дереве");
            res = null;
            if (target < current.data)
            {
                if (target == current.data)
                {
                    res = current;
                    return current;
                }
                else
                {
                    return Find(target, current.left,ref res);
                }
                    
            }
            else
            {
                if (target == current.data)
                {
                    res = current;
                    return current;
                }
                else
                    return Find(target, current.right,ref res);
            }

        }
        private int Max(int l, int r)
        {
            return l > r ? l : r;
        }
        private int GetHeight(AVLNode current)
        {
            int height = 0;
            if (current != null)
            {
                int l = GetHeight(current.left);
                int r = GetHeight(current.right);
                int m = Max(l, r);
                height = m + 1;
            }
            return height;
        }
        public int GetHeight()
        {
            AVLNode current = Root;
            int height = 0;
            if (current != null)
            {
                int l = GetHeight(current.left);
                int r = GetHeight(current.right);
                int m = Max(l, r);
                height = m + 1;
            }
            return height;
        }
        private int BalanceFactor(AVLNode current)
        {
            int l = GetHeight(current.left);
            int r = GetHeight(current.right);
            int BFactor = l - r;
            return BFactor;
        }
        private AVLNode RotateRR(AVLNode parent)
        {
            AVLNode pivot = parent.right;
            parent.right = pivot.left;
            pivot.left = parent;
            return pivot;
        }
        private AVLNode RotateLL(AVLNode parent)
        {
            AVLNode pivot = parent.left;
            parent.left = pivot.right;
            pivot.right = parent;
            return pivot;
        }
        private AVLNode RotateLR(AVLNode parent)
        {
            AVLNode pivot = parent.left;
            parent.left = RotateRR(pivot);
            return RotateLL(parent);
        }
        private AVLNode RotateRL(AVLNode parent)
        {
            AVLNode pivot = parent.right;
            parent.right = RotateLL(pivot);
            return RotateRR(parent);
        }
        public void Delete()
        {
            root = null;
        }
    }
}
