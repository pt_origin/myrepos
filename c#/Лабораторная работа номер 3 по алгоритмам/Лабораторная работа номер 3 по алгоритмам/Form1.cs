﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Лабораторная_работа_номер_3_по_алгоритмам
{
    public partial class Form1 : Form
    {
        AVLTree tree = new AVLTree();
        public Form1()
        {
            InitializeComponent();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }
        private void Upd()
        {
            View.Nodes.Clear();
            EmptyLabel.Text = "IsEmpty = " + tree.IsEmpty() + ";";
            HeightLabel.Text = "Высота дерева = " + tree.GetHeight() + ";";
            if (!tree.IsEmpty())
            {
                TreeNode rootNode = new TreeNode(tree.Root.data.ToString());
                View.Nodes.Add(rootNode);
                Fill(rootNode,tree.Root);
                View.ExpandAll();
            }
            
        }
        private void Fill(TreeNode tr, AVLNode atr)
        {
            if ( atr.left != null)
            {
                TreeNode newNode = new TreeNode("Left: " + atr.left.data.ToString());
                tr.Nodes.Add(newNode);
                Fill(newNode, atr.left);
            }
            if (atr.right != null)
            {
                TreeNode newNode = new TreeNode("Right: " + atr.right.data.ToString());
                tr.Nodes.Add(newNode);
                Fill(newNode, atr.right);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Upd();
            
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (AddBox.Text == "")
            {
                MessageBox.Show("\nВы ничего не ввели", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!int.TryParse(AddBox.Text, out int x))
            {
                MessageBox.Show("\nКлюч  должен быть в виде цифры", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            tree.Add(x);
            Upd();
            AddBox.Clear();
        }

        private void AddBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void Delete_Click(object sender, EventArgs e)
        {

            if (DeleteBox.Text == "")
            {
                MessageBox.Show("\nВы ничего не ввели", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!int.TryParse(DeleteBox.Text, out int x))
            {
                MessageBox.Show("\nКлюч  должен быть в виде цифры", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            AVLNode temp = null;
            int Temp;
            try
            {
                temp = tree.Find(x);
                //MessageBox.Show(tree.Show(temp), "Find", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
                Temp = temp.data;
                temp = null;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("\nЭлемента нет в дереве", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            tree.Delete(x);
            Upd();
            DeleteBox.Clear();
            MessageBox.Show("\nЭлемент со значением = " + Temp.ToString() + " удален", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void Find_Click(object sender, EventArgs e)
        {
            if (FindBox.Text == "")
            {
                MessageBox.Show("\nВы ничего не ввели", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!int.TryParse(FindBox.Text, out int x))
            {
                MessageBox.Show("\nКлюч  должен быть в виде цифры", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                AVLNode temp = tree.Find(x);
                MessageBox.Show(tree.Show(temp), "Find", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show("\nЭлемента нет в дереве", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
            //if (temp != null)
            //{

            //    MessageBox.Show(tree.Show(temp), "Find", MessageBoxButtons.OK, MessageBoxIcon.Information);


            //}
            //else MessageBox.Show("\nЭлемента нет в дереве", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            FindBox.Clear();
        }

        private void OpenFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog1 = new OpenFileDialog())
            {
                openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    tree.Delete();
                    try
                    {
                        using (StreamReader streamReader = new StreamReader(openFileDialog1.FileName, System.Text.Encoding.GetEncoding(1251)))
                        {

                            string line;
                            while ((line = streamReader.ReadLine()) != null)
                            {
                                if (!int.TryParse(line, out int x))
                                {
                                    MessageBox.Show("\nКлюч  должен быть в виде цифры", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }

                                tree.Add(x);



                            }
                        }
                        Upd();

                    }
                    catch (FileNotFoundException ex)
                    {
                        tree.Delete();
                        MessageBox.Show(ex.Message + "Проверьте правильность имени файла");
                        return;
                    }
                    catch (InvalidDataException ex)
                    {
                        tree.Delete();
                        MessageBox.Show(ex.Message);
                        return;
                    }
                }
            }
        }

        private void DeleteAllTreeButton_Click(object sender, EventArgs e)
        {
            tree.Delete();
            Upd();
        }
    }
}
