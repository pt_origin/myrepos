﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная_работа_номер_3_по_алгоритмам
{
    class RedBlackTree
    {
        public NodeForRBTree Root;
        public RedBlackTree(int data,string val)
        {
            this.Root = new NodeForRBTree(data,val);
        }
        protected NodeForRBTree AddNode(NodeForRBTree to, int key,string val)
        {

            if (key == to.key)//если такой элемент уже есть, увеличиваем счетчик
            {

                to.count++;
                return to;

            }

            if (key < to.key) //нужно добавлять влево
            {
                if (to.left != null) //там уже есть элемент
                    return this.AddNode(to.left, key,val);
                return to.left = new NodeForRBTree(key,val, to);

            }
            else //добавление вправо
            {
                if (to.right != null) //там уже есть элемент
                    return this.AddNode(to.right, key, val);

                return to.right = new NodeForRBTree(key, val, to);

            }

        }

        public NodeForRBTree Add(int key,string val)
        {

            //добавим в дерево элемент n как в обычное бинарное дерево
            NodeForRBTree n = this.AddNode(this.Root, key, val);
            //необходимо нормализовать правила красно-черных деревьев
            this.Case1(ref n);
            //возвращаем добавленный элемент
            return n;

        }

        protected void Case1(ref NodeForRBTree n)
        {
            if (n.parent == null)
            {
                n.isRed = false;
                return;
            }
            this.Case2(ref n);
        }

        protected void Case2(ref NodeForRBTree n)
        {
            if (n.parent.isRed == false) //Предок текущего узла черный, свойство 4 не нарушается.
            {
                n.isRed = true;
                return;
            }
            else this.Case3(ref n);
        }

        protected void Case3(ref NodeForRBTree n)
        {

            NodeForRBTree u = this.Uncle(n);//дядя

            NodeForRBTree g = this.Grandparent(n); //дедушка

            if ((u != null) && (u.isRed == true)) //Если и родитель и дядя красные
            {
                n.parent.isRed = false;
                u.isRed = false;
                g.isRed = true;
                this.Case1(ref g);//Чтобы это исправить, вся рекурсивно выполняется case1 для дедушки

            }
            else this.Case4(ref n);//Родитель P является красным, но дядя U — черный.

        }
        protected void Case4(ref NodeForRBTree n)
        { //тут два случая (4 и 5) объеденены в 1 метод, но возможно неправильно!

            NodeForRBTree g = this.Grandparent(n); //дедушка

            if (g == null)
                return;

            if ((n == n.parent.right) && (n.parent == g.left))
            {
                this.Rotate_left(ref n);//поворот дерева влево
            }
            else if ((n == n.parent.left) && (n.parent == g.right))
            {
                Rotate_right(ref n);//поворот вправо
            }
            // Родитель P является красным, но дядя U — черный, текущий узел N — левый\правый потомок P
            // и P — левый\правый потомок G.
            //цвета P и G меняются и в результате дерево удовлетворяет Свойству 4 (Оба потомка любого красного узла — черные).

            n.parent.isRed = false;
            g.isRed = true;

            //поворачиваем дедушку

            if ((n == n.parent.left) && (n.parent == g.left))
            {
                this.Rotate_right(ref g);

            } else
            {
                this.Rotate_left(ref g);
            }
        }

        //повоторот вправо

        protected void Rotate_right(ref NodeForRBTree n)
        {
            if (n.parent == null)
                return;
            //правый потомок узла ставим в левый родительского
            if (n.left != null)
            {
                n.parent.left = n.right;
                n.parent.left.parent = n.parent;
            }

            else n.parent.left = null;

            n.right = n.parent;//родительский ставим на его место

            n.parent = n.parent.parent;//ссылку на нового родителя берем у бывшего родителя

            n.right.parent = n;//бывшему родителю ставим ссылку на текущий узел

            if (n.parent != null)//если нужно поправить ссылку у дедушки
            {
                if (n.parent.left == n.left)
                    n.parent.left = n;
                if (n.parent.right == n.left)
                    n.parent.right = n;
            }
            else //корень дерева поменялся
                this.Root = n;
        }
        //поворот влево
        protected void Rotate_left(ref NodeForRBTree n)
        { 
            if (n.parent == null)
                return;
            if (n.left != null)
            {
                n.parent.right = n.left;
                n.parent.right.parent = n.parent;
            }
            else n.parent.right = null;
            n.left = n.parent;//родительский ставим на его место
            n.parent = n.parent.parent;//ссылку на нового родителя берем у бывшего родителя
            n.left.parent = n;//бывшему родителю ставим ссылку на текущий узел
            if (n.parent != null)//если нужно поправить ссылку у дедушки
            {
                if (n.parent.left == n.left)
                    n.parent.left = n;
                if (n.parent.right == n.left)
                    n.parent.right = n;
            }
            else //корень дерева поменялся
                this.Root = n;
        }

        protected NodeForRBTree Grandparent(NodeForRBTree n)
        {
            if ((n != null) && (n.parent != null))
            return n.parent.parent;
            else return null;
        }

        protected NodeForRBTree Uncle(NodeForRBTree n)
        {

            NodeForRBTree g = Grandparent(n);

            if (g == null)
                return null;

            if (n.parent == g.left)
                return g.right;
            else return g.left;

        }

        public void Delete(int key)
        {
            NodeForRBTree p = this.Root;
            //find node with key
            while ( p.key != key)
            {
                if (p.key < key)
                    p = p.right;
                else p = p.left;
               
            }
            if (p.right == null && p.left == null)
            {
                if (p == Root)
                    Root = null;
                else
                {
                    if (p.parent.right == p)
                        p.parent.right = null;
                    else p.parent.left = null; 
                }
                return;
                
            }

            NodeForRBTree y = null;
            NodeForRBTree q = null;
            if(p.left != null ^ p.right != null )
            {
                if (p.left != null)
                {
                    p.parent.left = null;
                }
                else p.parent.right = null;
            }
            else
            {

            }

            
        }


    }
}
