﻿namespace Лабораторная_работа_номер_3_по_алгоритмам
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.View = new System.Windows.Forms.TreeView();
            this.button1 = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.AddBox = new System.Windows.Forms.TextBox();
            this.DeleteBox = new System.Windows.Forms.TextBox();
            this.Delete = new System.Windows.Forms.Button();
            this.FindBox = new System.Windows.Forms.TextBox();
            this.Find = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.OpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteAllTreeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.EmptyLabel = new System.Windows.Forms.Label();
            this.HeightLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // View
            // 
            this.View.Location = new System.Drawing.Point(12, 33);
            this.View.Name = "View";
            this.View.Size = new System.Drawing.Size(461, 405);
            this.View.TabIndex = 0;
            this.View.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(305, 137);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(643, 88);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 2;
            this.Add.Text = "Add";
            this.Add.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // AddBox
            // 
            this.AddBox.Location = new System.Drawing.Point(504, 88);
            this.AddBox.Name = "AddBox";
            this.AddBox.Size = new System.Drawing.Size(100, 20);
            this.AddBox.TabIndex = 3;
            this.AddBox.TextChanged += new System.EventHandler(this.AddBox_TextChanged);
            // 
            // DeleteBox
            // 
            this.DeleteBox.Location = new System.Drawing.Point(504, 136);
            this.DeleteBox.Name = "DeleteBox";
            this.DeleteBox.Size = new System.Drawing.Size(100, 20);
            this.DeleteBox.TabIndex = 4;
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(643, 136);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(75, 23);
            this.Delete.TabIndex = 5;
            this.Delete.Text = "Delete";
            this.Delete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // FindBox
            // 
            this.FindBox.Location = new System.Drawing.Point(504, 183);
            this.FindBox.Name = "FindBox";
            this.FindBox.Size = new System.Drawing.Size(100, 20);
            this.FindBox.TabIndex = 6;
            // 
            // Find
            // 
            this.Find.Location = new System.Drawing.Point(643, 183);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(75, 23);
            this.Find.TabIndex = 7;
            this.Find.Text = "Find";
            this.Find.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Find.UseVisualStyleBackColor = true;
            this.Find.Click += new System.EventHandler(this.Find_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenFile});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // OpenFile
            // 
            this.OpenFile.Name = "OpenFile";
            this.OpenFile.Size = new System.Drawing.Size(98, 20);
            this.OpenFile.Text = "Открыть файл";
            this.OpenFile.Click += new System.EventHandler(this.OpenFile_Click);
            // 
            // DeleteAllTreeButton
            // 
            this.DeleteAllTreeButton.Location = new System.Drawing.Point(504, 234);
            this.DeleteAllTreeButton.Name = "DeleteAllTreeButton";
            this.DeleteAllTreeButton.Size = new System.Drawing.Size(214, 23);
            this.DeleteAllTreeButton.TabIndex = 9;
            this.DeleteAllTreeButton.Text = "Удалить";
            this.DeleteAllTreeButton.UseVisualStyleBackColor = true;
            this.DeleteAllTreeButton.Click += new System.EventHandler(this.DeleteAllTreeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(501, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Введите значение";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(501, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Введите значение";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(501, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Введите значение";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(534, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Операции с одним элементом";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(556, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Операции с деревом";
            // 
            // EmptyLabel
            // 
            this.EmptyLabel.AutoSize = true;
            this.EmptyLabel.Location = new System.Drawing.Point(504, 278);
            this.EmptyLabel.Name = "EmptyLabel";
            this.EmptyLabel.Size = new System.Drawing.Size(77, 13);
            this.EmptyLabel.TabIndex = 15;
            this.EmptyLabel.Text = "IsEmpty = true;";
            // 
            // HeightLabel
            // 
            this.HeightLabel.AutoSize = true;
            this.HeightLabel.Location = new System.Drawing.Point(504, 306);
            this.HeightLabel.Name = "HeightLabel";
            this.HeightLabel.Size = new System.Drawing.Size(105, 13);
            this.HeightLabel.TabIndex = 16;
            this.HeightLabel.Text = "Высота дерева = 0;";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.HeightLabel);
            this.Controls.Add(this.EmptyLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DeleteAllTreeButton);
            this.Controls.Add(this.View);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.FindBox);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.DeleteBox);
            this.Controls.Add(this.AddBox);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Ципощук Павел. Лабораторная работа номер 3 по алгоритмам";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView View;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.TextBox AddBox;
        private System.Windows.Forms.TextBox DeleteBox;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.TextBox FindBox;
        private System.Windows.Forms.Button Find;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem OpenFile;
        private System.Windows.Forms.Button DeleteAllTreeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label EmptyLabel;
        private System.Windows.Forms.Label HeightLabel;
    }
}

