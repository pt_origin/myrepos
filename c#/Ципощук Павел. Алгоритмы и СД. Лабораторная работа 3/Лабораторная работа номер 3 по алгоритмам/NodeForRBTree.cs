﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная_работа_номер_3_по_алгоритмам
{
    class NodeForRBTree
    {
        public NodeForRBTree left;
        public NodeForRBTree right;
        public NodeForRBTree parent;
        public bool isRed = true;
        public int key;
        public string Value;
        public int count;

        public NodeForRBTree(int data,string val)
        {
            this.key = data;
            this.left = null;
            this.right = null;
            this.parent = null;
            this.count = 1;
            this.Value = val;
        }

        public NodeForRBTree(int data, string val,NodeForRBTree parent ) : this(data,val)
        {
            this.parent = parent;
        }
    }
}
